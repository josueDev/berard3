//
//  DetallePolizas.swift
//  Berard
//
//  Created by Josué Gustavo Hernández Villa on 26/02/19.
//  Copyright © 2019 SinergiaDigital. All rights reserved.
//

import UIKit

class DetallePolizas: UIViewController , UITableViewDataSource , UITableViewDelegate{
    
    
    @IBOutlet var tableView: UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.register(UINib(nibName: "CeldaPoliza", bundle: nil), forCellReuseIdentifier: "CeldaPoliza")
        tableView.register(UINib(nibName: "CeldaPolizaDetalle", bundle: nil), forCellReuseIdentifier: "CeldaPolizaDetalle")
        
        let obje1 = ObjetoPoliza(id: "", titulo: "PROFESIONAL", poliza: "8932749827", compañia: "GNP", vigencia: "01/NOV/2018 - 01/NOV/2020", cobertura: "Amplia", estatus: "Vigente", tipo: "Vida")
        let obje2 = ObjetoPoliza(id: "", titulo: "PROYECTA", poliza: "8932749827", compañia: "GNP", vigencia: "01/NOV/2018 - 01/NOV/2020", cobertura: "Amplia", estatus: "Vigente", tipo: "Vida")
        ObjetoPoliza.getData().add(obje1)
        ObjetoPoliza.getData().add(obje2)
        
        let objeT = ObjetoPolizaDetalle(id: "49583085", l1: "GNP", l2: "Berard Agencia de seguros y finanzas ", l3: "Anual", l4: "Anual", l5: "Pesos", l6: "Amplia", l7: "Vigente", l8: "MIGUEL ANGEL FLORES", l9: "12/12/1970", l10: "12/12/1970", l11: "MIGUEL ANGEL FLORES", l12: "12/12/1970", l13: "12/12/1970", l14: "12/12/1970")
        ObjetoPolizaDetalle.getData().add(objeT)
        // Do any additional setup after loading the view.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if (indexPath.row == 0)
        {
            return 180.0;//Choose your custom row height
        }
        else
        {
            return 470.0;//Choose your custom row height
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if (indexPath.row == 0)
        {
            let CeldaPoliza = tableView.dequeueReusableCell(withIdentifier: "CeldaPoliza", for: indexPath) as! CeldaPoliza
            
            let obj = ObjetoPoliza.getData().object(at: indexPath.row) as! ObjetoPoliza
            
            let label1f1 = [NSAttributedString.Key.foregroundColor: color4, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14)]
            let label1f2 = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13)]
            let label1f3 = [NSAttributedString.Key.foregroundColor: color4, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14)]
            let label1f4 = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13)]
            let label1f5 = [NSAttributedString.Key.foregroundColor: color4, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14)]
            let label1f6 = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13)]
            
            let label1t1 = NSMutableAttributedString(string: "Póliza: ", attributes: label1f1)
            let label1t2 = NSMutableAttributedString(string: "2544353 \n" , attributes: label1f2)
            let label1t3 = NSMutableAttributedString(string: "Compañia: ", attributes: label1f3)
            let label1t4 = NSMutableAttributedString(string: "GNP \n", attributes: label1f4)
            let label1t5 = NSMutableAttributedString(string: "Vigencia: ", attributes: label1f5)
            let label1t6 = NSMutableAttributedString(string: "01/NOV/2018 - 01/NOV/2020", attributes: label1f6)
            
            let combination = NSMutableAttributedString()
            
            combination.append(label1t1)
            combination.append(label1t2)
            combination.append(label1t3)
            combination.append(label1t4)
            combination.append(label1t5)
            combination.append(label1t6)
            
            let label1t7 = NSMutableAttributedString(string: "Cobertura: ", attributes: label1f1)
            let label1t8 = NSMutableAttributedString(string: "Amplia \n\n" , attributes: label1f2)
            let label1t9 = NSMutableAttributedString(string: "Status: ", attributes: label1f1)
            let label1t10 = NSMutableAttributedString(string: "Vigente" , attributes: label1f2)
            let combination2 = NSMutableAttributedString()
            combination2.append(label1t7)
            combination2.append(label1t8)
            combination2.append(label1t9)
            combination2.append(label1t10)
            
            //CeldaPoliza.label1.attributedText = "combination"
            CeldaPoliza.label1.text = "PROFESIONAL"
            CeldaPoliza.label1.adjustsFontSizeToFitWidth = true
            CeldaPoliza.label2.attributedText = combination
            CeldaPoliza.label3.attributedText = combination2
            //CeldaPoliza.label2.adjustsFontSizeToFitWidth = true
            
            //cell.label1
            
            CeldaPoliza.layer.cornerRadius = 20
            CeldaPoliza.layer.masksToBounds = true
            //CeldaPoliza.layer.borderWidth = 10
            //CeldaPoliza.clipsToBounds = true
            
            switch tipoMenuSelect  {
            case 1:
                CeldaPoliza.colorLabel.backgroundColor = color1
                CeldaPoliza.colorImage.image = UIImage(named: "Recurso 48.png")
            case 2:
                CeldaPoliza.colorLabel.backgroundColor = color2
                CeldaPoliza.colorImage.image = UIImage(named: "Recurso 48.png")
            case 3:
                CeldaPoliza.colorLabel.backgroundColor = color3
                CeldaPoliza.colorImage.image = UIImage(named: "Recurso 48.png")
            case 4:
                CeldaPoliza.colorLabel.backgroundColor = color4
                CeldaPoliza.colorImage.image = UIImage(named: "Recurso 48.png")
            case 5:
                CeldaPoliza.colorLabel.backgroundColor = color5
                CeldaPoliza.colorImage.image = UIImage(named: "Recurso 48.png")
            case 6:
                CeldaPoliza.colorLabel.backgroundColor = color6
                CeldaPoliza.colorImage.image = UIImage(named: "Recurso 48.png")
            default:
                CeldaPoliza.colorLabel.backgroundColor = color1
                CeldaPoliza.colorImage.image = UIImage(named: "Recurso 48.png")
            }
            
             return CeldaPoliza
        }
        else //(indexPath.row == 1)
        {
             let obj = ObjetoPolizaDetalle.getData().object(at: indexPath.row - 1) as! ObjetoPolizaDetalle
            let cell = tableView.dequeueReusableCell(withIdentifier: "CeldaPolizaDetalle", for: indexPath) as! CeldaPolizaDetalle
            
            cell.label1.text = obj.l1
            cell.label2.text = obj.l2
            cell.label3.text = obj.l3
            cell.label4.text = obj.l4
            cell.label5.text = obj.l5
            cell.label6.text = obj.l6
            cell.label7.text = obj.l7
            cell.label8.text = obj.l8
            cell.label9.text = obj.l9
            cell.label10.text = obj.l10
            cell.label11.text = obj.l11
            cell.label12.text = obj.l12
            cell.label13.text = obj.l13
            cell.label14.text = obj.l14
            
            switch tipoMenuSelect  {
            case 1:
                cell.colorLabel.backgroundColor = color1
                cell.colorImage.image = UIImage(named: "Recurso 48.png")
            case 2:
                cell.colorLabel.backgroundColor = color2
                cell.colorImage.image = UIImage(named: "Recurso 48.png")
            case 3:
                cell.colorLabel.backgroundColor = color3
                cell.colorImage.image = UIImage(named: "Recurso 48.png")
            case 4:
                cell.colorLabel.backgroundColor = color4
                cell.colorImage.image = UIImage(named: "Recurso 48.png")
            case 5:
                cell.colorLabel.backgroundColor = color5
                cell.colorImage.image = UIImage(named: "Recurso 48.png")
            case 6:
                cell.colorLabel.backgroundColor = color6
                cell.colorImage.image = UIImage(named: "Recurso 48.png")
            default:
                cell.colorLabel.backgroundColor = color6
                cell.colorImage.image = UIImage(named: "Recurso 48.png")
            }
            
            return cell
        }
        
        
       
        
        //let texto = "Nombre del plan \n Vida Conjuntas \n Plazo \n Desde \n Hasta \n Importe de ahorro \n Importe de protección \n Beneficios"
        //cell.labelIzquierda.text = texto
        
        //let texto2 = "Nombre del plan \n Vida Conjuntas \n Plazo \n Desde \n Hasta \n Importe de ahorro \n Importe de protección \n Beneficios"
        //cell.labelDerecha.text = texto2
        //cell.labelDerecha.adjustsFontSizeToFitWidth = true
        //cell.labelDerecha.backgroundColor = UIColor.red
        
        
    }    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
