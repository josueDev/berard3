//
//  ViewController.swift
//  TASXI
//
//  Created by Josué :D on 08/05/18.
//  Copyright © 2018 SinergiaDigital. All rights reserved.
//

import UIKit

import Alamofire
import SwiftyJSON
import PKHUD

class Home: UIViewController {
    

    @IBOutlet var user: UITextField!
    @IBOutlet var password: UITextField!
    @IBOutlet var login: UIButton!
    @IBOutlet var forgotPassword: UIButton!
    
    
    @IBOutlet var botonContacto: UIButton!
    
    
    @IBOutlet var botonConvenio: UIButton!
    

    @IBOutlet var botonServicios: UIButton!
    
    
    @IBOutlet var acceso: UILabel!
    
    
    @IBOutlet var iniciarSesion: UIButton!
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        UIApplication.shared.statusBarStyle = .default
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Show the navigation bar on other view controllers
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        // Remove title back button
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let result = String(describing: UserDefaults.standard.value(forKey: DataPersistent.TERMINAL_LOGIN)!)
//        if result == "true"
//        {
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let pushController = storyboard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
//            self.navigationController?.pushViewController(pushController, animated: true)
//        }

        //_ = LoginButton(readPermissions: [ .publicProfile ])
        
        //createAccount.addTarget(self, action: #selector(self.pushRegister), for: UIControlEvents.touchUpInside)
        botonContacto.addTarget(self, action: #selector(self.pushContacto), for: UIControl.Event.touchUpInside)
        botonConvenio.addTarget(self, action: #selector(self.pushConvenios), for: UIControl.Event.touchUpInside)
        iniciarSesion.addTarget(self, action: #selector(self.pushRegister), for: UIControl.Event.touchUpInside)
        
        iniciarSesion.layer.cornerRadius = 10
        iniciarSesion.clipsToBounds = true
        
        
         botonConvenio.imageView?.contentMode = .scaleAspectFit
         botonServicios.imageView?.contentMode = .scaleAspectFit
        
        botonContacto.imageView?.contentMode = .scaleAspectFit
        
        
        let result = String(describing: UserDefaults.standard.value(forKey: DataPersistent.TERMINAL_LOGIN)!)
        
        if result == "true"
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let pushController = storyboard.instantiateViewController(withIdentifier: "Menu") as! Menu
            self.navigationController?.pushViewController(pushController, animated: true)
        }
        else
        {

        }

        
        
//        let loginManager = LoginManager()
//        if(FBSDKAccessToken.currentAccessToken() == nil)
//        {
//            print("not logged in")
//        }
//        else{
//            print("logged in already")
//        }
       
    }
    
    @objc func pushContacto()
    {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pushController = storyboard.instantiateViewController(withIdentifier: "VistaTelefonos") as! VistaTelefonos
        self.navigationController?.pushViewController(pushController, animated: true)
        
        //UserDefaults.standard.setValue("true", forKey: DataPersistent.TERMINAL_LOGIN)
        
        //let result = String(describing: UserDefaults.standard.value(forKey: DataPersistent.TERMINAL_LOGIN)!)
        //if result == "true"
        
        //        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //        let pushController = storyboard.instantiateViewController(withIdentifier: "RegistryViewController") as! RegistryViewController
        //        self.navigationController?.pushViewController(pushController, animated: true)
    }

    @objc func pushRegister()
    {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pushController = storyboard.instantiateViewController(withIdentifier: "Menu") as! Menu
        self.navigationController?.pushViewController(pushController, animated: true)
        
        UserDefaults.standard.setValue("true", forKey: DataPersistent.TERMINAL_LOGIN)
        
        //let result = String(describing: UserDefaults.standard.value(forKey: DataPersistent.TERMINAL_LOGIN)!)
        //if result == "true"
        
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let pushController = storyboard.instantiateViewController(withIdentifier: "RegistryViewController") as! RegistryViewController
//        self.navigationController?.pushViewController(pushController, animated: true)
    }
    
    @objc func pushConvenios()
    {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pushController = storyboard.instantiateViewController(withIdentifier: "MenuSwichViewController") as! MenuSwichViewController
        self.navigationController?.pushViewController(pushController, animated: true)
    }
    
    @objc func pushMap()
    {
        
        if ((user.text != "") && (password.text != "") )
        {
            //loginUserAndPassword()
            
//            let token = Messaging.messaging().fcmToken
//            print("FCM token: \(token ?? "")")
//            
//            Messaging.messaging().subscribe(toTopic: "news") { error in
//                print("Subscribed to news topic")
//            }
//            
//            UserDefaults.standard.setValue("true", forKey: DataPersistent.TERMINAL_LOGIN)
//            UserDefaults.standard.synchronize()
//            print(String(describing: UserDefaults.standard.value(forKey: DataPersistent.TERMINAL_LOGIN)!))
//            
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let pushController = storyboard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
//            self.navigationController?.pushViewController(pushController, animated: true)
//            
//            UserDefaults.standard.setValue("true", forKey: DataPersistent.TERMINAL_LOGIN)
//            UserDefaults.standard.synchronize()
        }
        else
        {
            let alert = UIAlertController(title: "Taxsi", message: "Por favor no deje los campos de usuario y contraseña vacíos", preferredStyle: .alert)
            let actionOK : UIAlertAction = UIAlertAction(title: "Deacuerdo", style: .default, handler: nil)
            alert.addAction(actionOK)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

//    func loginUserAndPassword()
//    {
//        HUD.show(.progress)
//        
//        let parameters: Parameters = [
//
//            "tipo" : "2",
//            "email": user.text! as Any,
//            "password": password.text! as Any
//
//        ]
//
//        Alamofire.request(ApiDefinition.WS_LOGIN, method:  .get, parameters : parameters, encoding: URLEncoding.default).validate().responseJSON { response in
//            
//            
//            switch response.result {
//         
//            case .success(let value):
//                let json = JSON(value)
//                print("JSON: \(json)")
//                let pasajero = json["pasajero"]
//                let id = pasajero[0]["id"].string!
//                //let nombre: String = json["pasajero"]["nombre"].stringValue
//                //print("id " + id)
//                if id != "0"
//                {
//                    HUD.flash(.success, delay: 0.5)
//                    
//                    let id = pasajero[0]["id"].string!
//                    let nombre = pasajero[0]["nombre"].string!
//                    let apellidos = pasajero[0]["apellidos"].string!
//                    let edad = pasajero[0]["edad"].string!
//                    let emailRes = pasajero[0]["email"].string!
//                    let sexo = pasajero[0]["sexo"].string!
//                    let telefono = pasajero[0]["telefono"].string!
//                    
//                    UserDefaults.standard.setValue("true", forKey: DataPersistent.TERMINAL_LOGIN)
//                    UserDefaults.standard.setValue(id, forKey: DataPersistent.id)
//                    UserDefaults.standard.setValue(nombre, forKey: DataPersistent.nombre)
//                    UserDefaults.standard.setValue(edad, forKey: DataPersistent.edad)
//                    UserDefaults.standard.setValue(apellidos, forKey: DataPersistent.apellidos)
//                    UserDefaults.standard.setValue(emailRes, forKey: DataPersistent.email)
//                    UserDefaults.standard.setValue(sexo, forKey: DataPersistent.sexo)
//                    UserDefaults.standard.setValue(telefono, forKey: DataPersistent.telefono)
//                    UserDefaults.standard.synchronize()
//                    
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                        let pushController = storyboard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
//                        self.navigationController?.pushViewController(pushController, animated: true)
//                        
//
//                    }
//                }
//                else
//                {
//                    HUD.flash(.error, delay: 0.5)
//                }
//                
//            case .failure(let error):
//                print(error)
//                HUD.flash(.error, delay: 0.5)
//            }
//        }
//    }


}

