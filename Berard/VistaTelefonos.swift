//
//  VistaTelefonos.swift
//  Berard
//
//  Created by Josué :D on 27/03/19.
//  Copyright © 2019 SinergiaDigital. All rights reserved.
//

import UIKit

class VistaTelefonos: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {

    @IBOutlet var collectionView1: UICollectionView!
    
    @IBOutlet var collectionView2: UICollectionView!
    
    @IBOutlet var labelGris1: UILabel!
    
    @IBOutlet var labelGris2: UILabel!
    
    @IBOutlet var switch1: UISwitch!
    
    @IBOutlet var switch2: UISwitch!
    
    var backbutton = UIButton()
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let firstFrame = CGRect(x: 0, y: 0, width:1000, height: 45)
        let firstLabel = UILabel(frame: firstFrame)
        
        let nombre = String(describing: UserDefaults.standard.value(forKey: DataPersistent.nombre)!)
        firstLabel.text = "Hola \(nombre)"
        firstLabel.textAlignment = .center
        firstLabel.adjustsFontSizeToFitWidth = true
        self.navigationItem.titleView = firstLabel
        self.navigationController?.navigationBar.tintColor = UIColor(red: 255/255, green: 266/255, blue: 255/255, alpha: 1) // title and icon back
        navigationController?.navigationBar.barTintColor =  UIColor(red: 132/255, green: 25/255, blue: 19/255, alpha: 1)
        //self.navigationController!.navigationBar.titleTextAttributes = [ NSAttributedStringKey.foregroundColor: UIColor(red: 7/255, green: 7/255, blue: 7/255, alpha: 1)] // titleBar
        let leftBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "menubar.png"), style: .done, target: self, action: #selector(gomenu))
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
        UIApplication.shared.statusBarStyle = .lightContent
        
        let rightBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "power"), style: .done, target: self, action: #selector(backmenu))
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
        
        backbutton = UIButton(type: .system)
        backbutton.addTarget(self, action: #selector(backmenu), for: UIControl.Event.touchUpInside)
        backbutton.setImage(UIImage(named: "back"), for: .normal) // Image can be downloaded from here below link
        ///backbutton.setTitle("Back", forState: .Normal)
        backbutton.tintColor =  UIColor(red: 255/255, green: 266/255, blue: 255/255, alpha: 1)        //backbutton.setTitleColor(backbutton.tintColor, for: .normal) // You can change the TitleColor
        //backbutton.addTarget(self, action: "backAction", forControlEvents: .TouchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: backbutton)
        
        let image = UIImage(named: "white")!
        let imageSize = CGRect(x: 0, y: 0, width:1000, height: 25)
        let marginX: CGFloat = (self.navigationController!.navigationBar.frame.size.width / 2) - (imageSize.width / 2)
        let imageView = UIImageView(frame: CGRect(x: marginX, y: 0, width: imageSize.width, height: imageSize.height))
        imageView.image = image
        imageView.contentMode = .scaleAspectFit
        
        self.navigationItem.titleView = imageView
    }
    
    @objc func gomenu()
    {
        
    }
    
    @objc func backmenu()
    {
        navigationController?.popViewController(animated: true)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView1.delegate = self
        collectionView1.dataSource = self
        
        collectionView2.delegate = self
        collectionView2.dataSource = self
        
        collectionView1.register(UINib(nibName: "CeldaTelefono", bundle: nil), forCellWithReuseIdentifier: "CeldaTelefono")
        collectionView2.register(UINib(nibName: "CeldaTelefono", bundle: nil), forCellWithReuseIdentifier: "CeldaTelefono")
        
        collectionView1.register(UINib(nibName: "CeldaHeader", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier:"CeldaHeader")
        
        collectionView1.register(UINib(nibName: "CeldaTelefonoUno", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier:"CeldaTelefonoUno")
        collectionView2.register(UINib(nibName: "CeldaTelefonoUno", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier:"CeldaTelefonoUno")
        
        collectionView1.register(UINib(nibName: "CeldaTelefonoUno", bundle: nil), forCellWithReuseIdentifier: "CeldaTelefonoUno")
        collectionView2.register(UINib(nibName: "CeldaTelefonoUno", bundle: nil), forCellWithReuseIdentifier: "CeldaTelefonoUno")
        
        
        
        collectionView2.register(UINib(nibName: "CeldaHeader", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier:"CeldaHeader")
     
        
        //collectionView.register(UINib(nibName: "CustomCell", bundle: nil), forCellWithReuseIdentifier: "cellId")
    
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.headerReferenceSize = CGSize(width: self.view.frame.size.width, height: 30)
        
         //collectionView.register(UINib.init(nibName: "NIB", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "Header")
        
       // collectionView1.register(Header.self, forSupplementaryViewOfKind:UICollectionElementKindSectionHeader, withReuseIdentifier: headerId)
        
        //self.collectionView1.register(TaskCell.self, forCellWithReuseIdentifier: "TaskCell")
        //self.register(UINib(nibName: "CollectionViewHeadCell", bundle: nil), forCellWithReuseIdentifier: HeadCellIdentifier)
        
        labelGris1.layer.cornerRadius = 10
        labelGris1.clipsToBounds = true
        
        labelGris2.layer.cornerRadius = 10
        labelGris2.clipsToBounds = true
        
        llenarTelefonosCompañia()
        
        switch1.setOn(true, animated: false)
        switch2.setOn(false, animated: false)
        
        collectionView1.isHidden = false
        collectionView2.isHidden = true

        // Do any additional setup after loading the view.
    }
    
    func llenarTelefonosCompañia()
    {
        let object = ObjetoTelefonoCompania(id: "", imagen: UIImage(named: "Recurso 80.png")!, texto: "asdf", telefono: "asdf")
        let object1 = ObjetoTelefonoCompania(id: "", imagen: UIImage(named: "Recurso 81.png")!, texto: "asdf", telefono: "asdf")
        let object2 = ObjetoTelefonoCompania(id: "", imagen: UIImage(named: "Recurso 82.png")!, texto: "asdf", telefono: "asdf")
        let object3 = ObjetoTelefonoCompania(id: "", imagen: UIImage(named: "Recurso 83.png")!, texto: "asdf", telefono: "asdf")
        ObjetoTelefonoCompania.getData().add(object1)
        ObjetoTelefonoCompania.getData().add(object2)
        ObjetoTelefonoCompania.getData().add(object3)
        ObjetoTelefonoCompania.getData().add(object)
        
        let objectF = ObjetoTelefonosOficina(id: "", imagen: UIImage(named: "Recurso 50.png")!, texto: "asdf", telefono: "asdf", tipo: 0)
        let objectF2 = ObjetoTelefonosOficina2(id: "", imagen: UIImage(named: "Recurso 50.png")!, texto: "asdf", telefono: "asdf", tipo: 1)
        let objectF3 = ObjetoTelefonosOficina3(id: "", imagen: UIImage(named: "Recurso 50.png")!, texto: "asdf", telefono: "asdf", tipo: 2)
        let objectF4 = ObjetoTelefonosOficina4(id: "", imagen: UIImage(named: "Recurso 50.png")!, texto: "asdf", telefono: "asdf", tipo: 3)
        
        ObjetoTelefonosOficina.getData().add(objectF)
        ObjetoTelefonosOficina2.getData().add(objectF2)
        ObjetoTelefonosOficina3.getData().add(objectF3)
        ObjetoTelefonosOficina4.getData().add(objectF4)
 
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        if (collectionView == collectionView1)
        {
            return CGSize(width:collectionView.frame.size.width, height:30)
        }
        else
        {
            return CGSize(width:collectionView.frame.size.width, height:0)
        }

       // return CGSize(width:collectionView.frame.size.width, height:30)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    //--------------------------------------------------------------------------------
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 20;
    }
    
    //--------------------------------------------------------------------------------
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //return CGSize(width: ( self.collectionView1.frame.size.width - 60 ) / 2,height:( self.collectionView1.frame.size.width - 60 ) / 2)
        
//        let width = collectionView.bounds.width
//        let cellWidth = (width - 1) / 2 // compute your cell width
//        return CGSize(width: cellWidth, height: cellWidth / 0.3)
        
        let width = (collectionView.frame.width - 8) / 4
        return CGSize(width: width, height: width)
        
        //return CGSize(width: 100.0, height: 100.0)
    }
    
    //--------------------------------------------------------------------------------
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
    }
    
//    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
//        //#warning Incomplete method implementation -- Return the number of sections
//        if (collectionView == collectionView1)
//        {
//            return 4
//        }
//        else
//        {
//            return 1
//        }
//    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if (collectionView == collectionView1)
        {
            return 4
        }
        else
        {
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (collectionView == collectionView1)
        {
           //return ObjetoTelefonosOficina.getData().count
            return 3
        }
        else
        {
             return ObjetoTelefonoCompania.getData().count
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if (collectionView == collectionView1)
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CeldaTelefonoUno",
                                                          for: indexPath) as! CeldaTelefonoUno
            switch indexPath.section
            {
            case 0:
                cell.label.textColor = color1
                cell.image.image = UIImage(named: "Recurso 56.png")
                case 1:
                cell.label.textColor = color2
                cell.image.image = UIImage(named: "Recurso 57.png")
                case 2:
                cell.label.textColor = color3
                cell.image.image = UIImage(named: "Recurso 58.png")
                case 3:
                cell.label.textColor = color4
                cell.image.image = UIImage(named: "Recurso 59.png")
            default:
                print("Fallback option")
            }
           
            cell.label.text = "Dolores Fuente"
            cell.label.adjustsFontSizeToFitWidth = true
            return cell
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CeldaTelefono",
                                                          for: indexPath) as! CeldaTelefono
            
            let objeto : ObjetoTelefonoCompania = ObjetoTelefonoCompania.getData().object(at: indexPath.row) as! ObjetoTelefonoCompania
            cell.imagen.image = objeto.imagen
            
            cell.imagen.layer.cornerRadius = 10
            cell.imagen.clipsToBounds = true
            
            return cell
        }
    }
    
//    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
//        switch kind {
//        case UICollectionView.elementKindSectionHeader:
//             let cell = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "CeldaTelefono", for: indexPath) as! CeldaTelefono
//            return cell
//        default:
//            return UICollectionReusableView()
//        }
//    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {

        //if kind.isEqual(UICollectionView.elementKindSectionHeader) {
            let cell = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "CeldaHeader", for: indexPath) as! CeldaHeader
            // let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CeldaTelefono",
            //                                               for: indexPath) as! CeldaTelefono
            //cell.title = "MyString "
            //cell.backgroundColor = UIColor.black
        
        if (indexPath.section == 0)
        {
            cell.label.text = "VIDA"
            cell.label.textColor = color1
        }
        else if (indexPath.section == 1)
        {
            cell.label.text = "AUTO"
            cell.label.textColor = color2
        }
        else if (indexPath.section == 2)
        {
            cell.label.text = "GASTOS MÉDICOS"
            cell.label.textColor = color3
        }
        else
        {
            cell.label.text = "DAÑOS Y FINANZAS"
            cell.label.textColor = color4
        }
        
        return cell
        //} else {
        //    return UICollectionReusableView()
       // }

    }
    
    
    @IBAction func s1(_ sender: Any) {
        if switch1.isOn {
            switch1.setOn(false, animated: true)
            switch2.setOn(true, animated: true)
            collectionView1.isHidden = true
            collectionView2.isHidden = false
        } else {
            switch1.setOn(true, animated: false)
            switch2.setOn(false, animated: false)
            collectionView1.isHidden = false
            collectionView2.isHidden = true
        }
    }
    
    @IBAction func s2(_ sender: Any) {
        if switch2.isOn {
            switch1.setOn(false, animated: true)
            switch2.setOn(true, animated: true)
            collectionView1.isHidden = true
            collectionView2.isHidden = false
        } else {
            switch1.setOn(true, animated: true)
            switch2.setOn(false, animated: true)
            collectionView1.isHidden = false
            collectionView2.isHidden = true
        }
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
