//
//  Polizas.swift
//  Berard
//
//  Created by Josué Gustavo Hernández Villa on 26/02/19.
//  Copyright © 2019 SinergiaDigital. All rights reserved.
//

import UIKit

class Polizas: UIViewController , UITableViewDelegate , UITableViewDataSource {

    
    @IBOutlet var imageTop: UIImageView!
    
    
    @IBOutlet var tableView: UITableView!
    
    
    @IBOutlet var buttonCamara: UIButton!
    
    var backbutton = UIButton()
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let firstFrame = CGRect(x: 0, y: 0, width:1000, height: 45)
        let firstLabel = UILabel(frame: firstFrame)
        
        let nombre = String(describing: UserDefaults.standard.value(forKey: DataPersistent.nombre)!)
        firstLabel.text = "Hola \(nombre)"
        firstLabel.textAlignment = .center
        firstLabel.adjustsFontSizeToFitWidth = true
        self.navigationItem.titleView = firstLabel
        self.navigationController?.navigationBar.tintColor = UIColor(red: 255/255, green: 266/255, blue: 255/255, alpha: 1) // title and icon back
        navigationController?.navigationBar.barTintColor =  UIColor(red: 132/255, green: 25/255, blue: 19/255, alpha: 1)
        //self.navigationController!.navigationBar.titleTextAttributes = [ NSAttributedStringKey.foregroundColor: UIColor(red: 7/255, green: 7/255, blue: 7/255, alpha: 1)] // titleBar
        let leftBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "menubar.png"), style: .done, target: self, action: #selector(gomenu))
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
        //UIApplication.shared.statusBarStyle = .default
        
        let rightBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "back"), style: .done, target: self, action: #selector(backmenu))
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
        //UIApplication.shared.statusBarStyle = .default
        
        backbutton = UIButton(type: .custom)
        backbutton.addTarget(self, action: #selector(backmenu), for: UIControl.Event.touchUpInside)
        backbutton.setImage(UIImage(named: "back"), for: .normal) // Image can be downloaded from here below link
        ///backbutton.setTitle("Back", forState: .Normal)
        //backbutton.setTitleColor(backbutton.tintColor, forState: .normal) // You can change the TitleColor
        //backbutton.addTarget(self, action: "backAction", forControlEvents: .TouchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: backbutton)
        
        let image = UIImage(named: "white")!
        let imageSize = CGRect(x: 0, y: 0, width:1000, height: 25)
        let marginX: CGFloat = (self.navigationController!.navigationBar.frame.size.width / 2) - (imageSize.width / 2)
        let imageView = UIImageView(frame: CGRect(x: marginX, y: 0, width: imageSize.width, height: imageSize.height))
        imageView.image = image
        imageView.contentMode = .scaleAspectFit
        
        self.navigationItem.titleView = imageView
    }
    
    @objc func gomenu()
    {
        
        let result = String(describing: UserDefaults.standard.value(forKey: DataPersistent.TERMINAL_LOGIN)!)
        
        if result == "true"
        {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: Menu.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        }
        else
        {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: Home.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
            
        }
    }
    
    @objc func backmenu()

    {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "CeldaPoliza", bundle: nil), forCellReuseIdentifier: "CeldaPoliza")
        
        let obje1 = ObjetoPoliza(id: "", titulo: "PROFESIONAL", poliza: "8932749827", compañia: "GNP", vigencia: "01/NOV/2018 - 01/NOV/2020", cobertura: "Amplia", estatus: "Vigente", tipo: "Vida")
         let obje2 = ObjetoPoliza(id: "", titulo: "PROYECTA", poliza: "8932749827", compañia: "GNP", vigencia: "01/NOV/2018 - 01/NOV/2020", cobertura: "Amplia", estatus: "Vigente", tipo: "Vida")
        ObjetoPoliza.getData().add(obje1)
        ObjetoPoliza.getData().add(obje2)
        
        buttonCamara.imageView?.contentMode = .scaleAspectFit
        
        switch tipoMenuSelect  {
        case 1:
            imageTop.image = UIImage(named: "en1.png")
        case 2:
            imageTop.image = UIImage(named: "en2.png")
        case 3:
            imageTop.image = UIImage(named: "en3.png")
        case 4:
            imageTop.image = UIImage(named: "en4.png")
        case 5:
            imageTop.image = UIImage(named: "en5.png")
        case 6:
            imageTop.image = UIImage(named: "en6.png")
        default:
            imageTop.image = UIImage(named: "en6.png")
        }
    

        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ObjetoPoliza.getData().count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 180.0;//Choose your custom row height
    }
    

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let CeldaPoliza = tableView.dequeueReusableCell(withIdentifier: "CeldaPoliza", for: indexPath) as! CeldaPoliza
        
        let obj = ObjetoPoliza.getData().object(at: indexPath.row) as! ObjetoPoliza
        
        let label1f1 = [NSAttributedString.Key.foregroundColor: color4, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14)]
        let label1f2 = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13)]
        let label1f3 = [NSAttributedString.Key.foregroundColor: color4, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14)]
        let label1f4 = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13)]
        let label1f5 = [NSAttributedString.Key.foregroundColor: color4, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14)]
        let label1f6 = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13)]
        
        let label1t1 = NSMutableAttributedString(string: "Póliza: ", attributes: label1f1)
        let label1t2 = NSMutableAttributedString(string: "2544353 \n" , attributes: label1f2)
        let label1t3 = NSMutableAttributedString(string: "Compañia: ", attributes: label1f3)
        let label1t4 = NSMutableAttributedString(string: "GNP \n", attributes: label1f4)
        let label1t5 = NSMutableAttributedString(string: "Vigencia: ", attributes: label1f5)
        let label1t6 = NSMutableAttributedString(string: "01/NOV/2018 - 01/NOV/2020", attributes: label1f6)
        
        let combination = NSMutableAttributedString()
        
        combination.append(label1t1)
        combination.append(label1t2)
        combination.append(label1t3)
        combination.append(label1t4)
        combination.append(label1t5)
        combination.append(label1t6)
        
        let label1t7 = NSMutableAttributedString(string: "Cobertura: ", attributes: label1f1)
        let label1t8 = NSMutableAttributedString(string: "Amplia \n\n" , attributes: label1f2)
        let label1t9 = NSMutableAttributedString(string: "Status: ", attributes: label1f1)
        let label1t10 = NSMutableAttributedString(string: "Vigente" , attributes: label1f2)
        let combination2 = NSMutableAttributedString()
        combination2.append(label1t7)
        combination2.append(label1t8)
        combination2.append(label1t9)
        combination2.append(label1t10)
        
        //CeldaPoliza.label1.attributedText = "combination"
        CeldaPoliza.label1.text = "PROFESIONAL"
        CeldaPoliza.label1.adjustsFontSizeToFitWidth = true
        CeldaPoliza.label2.attributedText = combination
        CeldaPoliza.label3.attributedText = combination2
        //CeldaPoliza.label2.adjustsFontSizeToFitWidth = true
        
        //cell.label1
        
        CeldaPoliza.layer.cornerRadius = 20
        CeldaPoliza.layer.masksToBounds = true
        //CeldaPoliza.layer.borderWidth = 10
        //CeldaPoliza.clipsToBounds = true
        
        switch tipoMenuSelect  {
        case 1:
            CeldaPoliza.colorLabel.backgroundColor = color1
            CeldaPoliza.colorImage.image = UIImage(named: "Recurso 48.png")
        case 2:
            CeldaPoliza.colorLabel.backgroundColor = color2
                CeldaPoliza.colorImage.image = UIImage(named: "Recurso 48.png")
        case 3:
            CeldaPoliza.colorLabel.backgroundColor = color3
                CeldaPoliza.colorImage.image = UIImage(named: "Recurso 48.png")
        case 4:
            CeldaPoliza.colorLabel.backgroundColor = color4
                CeldaPoliza.colorImage.image = UIImage(named: "Recurso 48.png")
        case 5:
            CeldaPoliza.colorLabel.backgroundColor = color5
                CeldaPoliza.colorImage.image = UIImage(named: "Recurso 48.png")
        case 6:
            CeldaPoliza.colorLabel.backgroundColor = color6
                CeldaPoliza.colorImage.image = UIImage(named: "Recurso 48.png")
        default:
            CeldaPoliza.colorLabel.backgroundColor = color1
                CeldaPoliza.colorImage.image = UIImage(named: "Recurso 48.png")
        }
        
        
        
        return CeldaPoliza
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt
        indexPath: IndexPath){
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pushController = storyboard.instantiateViewController(withIdentifier: "DetallePolizas") as! DetallePolizas
        self.navigationController?.pushViewController(pushController, animated: true)
        
        //your code...
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

