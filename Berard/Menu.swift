//
//  Menu.swift
//  Berard
//
//  Created by Josué Gustavo Hernández Villa on 25/02/19.
//  Copyright © 2019 SinergiaDigital. All rights reserved.
//

import UIKit

//50 6 3
var tipoMenuSelect = 0

class Menu: UIViewController {
    
    
    @IBOutlet var b1: UIButton!
    
    @IBOutlet var b2: UIButton!
    
    @IBOutlet var b3: UIButton!
    
    @IBOutlet var b4: UIButton!
    
    @IBOutlet var b5: UIButton!
    
    @IBOutlet var b6: UIButton!
    
    @IBOutlet var bar1: UIBarButtonItem!
    
    @IBOutlet var bar2: UIBarButtonItem!
    
    @IBOutlet var bar3: UIBarButtonItem!
    
    @IBOutlet var gastos: UILabel!
    
    @IBOutlet var daños: UILabel!
    
    
    var backbutton = UIButton()
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let firstFrame = CGRect(x: 0, y: 0, width:1000, height: 45)
        let firstLabel = UILabel(frame: firstFrame)
        
        let nombre = String(describing: UserDefaults.standard.value(forKey: DataPersistent.nombre)!)
        firstLabel.text = "Hola \(nombre)"
        firstLabel.textAlignment = .center
        firstLabel.adjustsFontSizeToFitWidth = true
        self.navigationItem.titleView = firstLabel
        self.navigationController?.navigationBar.tintColor = UIColor(red: 255/255, green: 266/255, blue: 255/255, alpha: 1) // title and icon back
        navigationController?.navigationBar.barTintColor =  UIColor(red: 132/255, green: 25/255, blue: 19/255, alpha: 1)
        //self.navigationController!.navigationBar.titleTextAttributes = [ NSAttributedStringKey.foregroundColor: UIColor(red: 7/255, green: 7/255, blue: 7/255, alpha: 1)] // titleBar
        let leftBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "transparente"), style: .done, target: self, action: #selector(gomenu))
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
        //UIApplication.shared.statusBarStyle = .default
        
        let rightBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "back"), style: .done, target: self, action: #selector(backmenu))
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
        //UIApplication.shared.statusBarStyle = .default
        
        backbutton = UIButton(type: .custom)
        backbutton.addTarget(self, action: #selector(backmenu), for: UIControl.Event.touchUpInside)
        backbutton.setImage(UIImage(named: "back"), for: .normal) // Image can be downloaded from here below link
        ///backbutton.setTitle("Back", forState: .Normal)
        //backbutton.setTitleColor(backbutton.tintColor, forState: .normal) // You can change the TitleColor
        //backbutton.addTarget(self, action: "backAction", forControlEvents: .TouchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: backbutton)
        
        let image = UIImage(named: "white")!
        let imageSize = CGRect(x: 0, y: 0, width:1000, height: 25)
        let marginX: CGFloat = (self.navigationController!.navigationBar.frame.size.width / 2) - (imageSize.width / 2)
        let imageView = UIImageView(frame: CGRect(x: marginX, y: 0, width: imageSize.width, height: imageSize.height))
        imageView.image = image
        imageView.contentMode = .scaleAspectFit
        
        self.navigationItem.titleView = imageView
    }
    @objc func gomenu()
    {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: Home.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
        //UserDefaults.standard.setValue("true", forKey: DataPersistent.TERMINAL_LOGIN)
    }
    
    @objc func backmenu()
    {
//        for controller in self.navigationController!.viewControllers as Array {
//            if controller.isKind(of: Home.self) {
//                self.navigationController!.popToViewController(controller, animated: true)
//                break
//            }
//        }

        
        // Create the alert controller
        let alertController = UIAlertController(title: "Berard", message: "¿Está seguro que desea cerrar su sesión?", preferredStyle: .alert)
    
        let cancelAction = UIAlertAction(title: "Cancelar", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        
        // Create the actions
        let okAction = UIAlertAction(title: "Deacuerdo", style: UIAlertAction.Style.default) {
            UIAlertAction in
    
        
            if let navController = self.navigationController {
                UserDefaults.standard.setValue("false", forKey: DataPersistent.TERMINAL_LOGIN)
                navController.popViewController(animated: true)
            }
            

        }

        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        b1.imageView?.contentMode = .scaleAspectFit
         b2.imageView?.contentMode = .scaleAspectFit
         b3.imageView?.contentMode = .scaleAspectFit
         b4.imageView?.contentMode = .scaleAspectFit
         b5.imageView?.contentMode = .scaleAspectFit
         b6.imageView?.contentMode = .scaleAspectFit
        
        
        b1.addTarget(self, action: #selector(self.push1), for: UIControl.Event.touchUpInside)
        b2.addTarget(self, action: #selector(self.push2), for: UIControl.Event.touchUpInside)
        b3.addTarget(self, action: #selector(self.push3), for: UIControl.Event.touchUpInside)
        b4.addTarget(self, action: #selector(self.push4), for: UIControl.Event.touchUpInside)
        b5.addTarget(self, action: #selector(self.push5), for: UIControl.Event.touchUpInside)
        b6.addTarget(self, action: #selector(self.push6), for: UIControl.Event.touchUpInside)        // Do any additional setup after loading the view.
        
        gastos.adjustsFontSizeToFitWidth = true
        daños.adjustsFontSizeToFitWidth = true
        
    }
    
    @objc func actionBar1()
    {
        
    }
    
    @objc func actionBar2()
    {
        
    }
    
    @objc func actionBar3()
    {
        
    }
    

    @objc func push1()
    {
        tipoMenuSelect = 1
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pushController = storyboard.instantiateViewController(withIdentifier: "Polizas") as! Polizas
        self.navigationController?.pushViewController(pushController, animated: true)

    }
    
    @objc func push2()
    {
        tipoMenuSelect = 2
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pushController = storyboard.instantiateViewController(withIdentifier: "Polizas") as! Polizas
        self.navigationController?.pushViewController(pushController, animated: true)
        
    }
    
    @objc func push3()
    {
        tipoMenuSelect = 3
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pushController = storyboard.instantiateViewController(withIdentifier: "Polizas") as! Polizas
        self.navigationController?.pushViewController(pushController, animated: true)
        
    }
    
    @objc func push4()
    {
        tipoMenuSelect = 4
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pushController = storyboard.instantiateViewController(withIdentifier: "Polizas") as! Polizas
        self.navigationController?.pushViewController(pushController, animated: true)
        
    }
    
    @objc func push5()
    {
        tipoMenuSelect = 5
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pushController = storyboard.instantiateViewController(withIdentifier: "Polizas") as! Polizas
        self.navigationController?.pushViewController(pushController, animated: true)
        
    }
    
    @objc func push6()
    {
        tipoMenuSelect = 6
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pushController = storyboard.instantiateViewController(withIdentifier: "Polizas") as! Polizas
        self.navigationController?.pushViewController(pushController, animated: true)
        
    }
    
    
    @IBAction func actionBar1(_ sender: Any) {
    }
    
    @IBAction func actionBar2(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pushController = storyboard.instantiateViewController(withIdentifier: "MenuSwichViewController") as! MenuSwichViewController
        self.navigationController?.pushViewController(pushController, animated: true)
        
    }
    
    @IBAction func actionBar3(_ sender: Any) {
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
