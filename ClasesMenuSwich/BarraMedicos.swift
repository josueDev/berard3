//
//  BarraMedicos.swift
//  Berard
//
//  Created by Josué Gustavo Hernández Villa on 13/02/19.
//  Copyright © 2019 SinergiaDigital. All rights reserved.
//

import UIKit

class BarraMedicos: UIViewController {
    
    
    @IBOutlet var gris: UILabel!
    
    @IBOutlet var titulo: UILabel!
    
    @IBOutlet var boton: UISwitch!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        gris.layer.cornerRadius = 10
        gris.clipsToBounds = true
        
        titulo.adjustsFontSizeToFitWidth = true
        titulo.text = "Médicos Especialistas   "

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
