//
//  Hospitales.swift
//  Berard
//
//  Created by Josué Gustavo Hernández Villa on 13/02/19.
//  Copyright © 2019 SinergiaDigital. All rights reserved.
//

import UIKit

class Hospitales: UIViewController {
    
    

    @IBOutlet var boton1: UIButton!
    
    
    @IBOutlet var boton2: UIButton!
    
    
    @IBOutlet var boton3: UIButton!
    
    @IBOutlet var boton4: UIButton!
    
    
    @IBOutlet var boton5: UIButton!
    
    @IBOutlet var boton6: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
    boton1.addTarget(self, action: #selector(self.push1), for: UIControl.Event.touchUpInside)
    boton2.addTarget(self, action: #selector(self.push2), for: UIControl.Event.touchUpInside)
        
    boton3.addTarget(self, action: #selector(self.push3), for: UIControl.Event.touchUpInside)
        
    boton4.addTarget(self, action: #selector(self.push4), for: UIControl.Event.touchUpInside)
        
    boton5.addTarget(self, action: #selector(self.push5), for: UIControl.Event.touchUpInside)
    boton6.addTarget(self, action: #selector(self.push6), for: UIControl.Event.touchUpInside)
        
         boton1.imageView?.contentMode = .scaleAspectFit
         boton2.imageView?.contentMode = .scaleAspectFit
         boton3.imageView?.contentMode = .scaleAspectFit
         boton4.imageView?.contentMode = .scaleAspectFit
         boton5.imageView?.contentMode = .scaleAspectFit
        boton6.imageView?.contentMode = .scaleAspectFit
        
        
        
        
        
        

        // Do any additional setup after loading the view.
    }
    
    @objc func push1()
    {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pushController = storyboard.instantiateViewController(withIdentifier: "ChristusMuguerza") as! ChristusMuguerza
        self.navigationController?.pushViewController(pushController, animated: true)
    }
    
    @objc func push2()
    {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pushController = storyboard.instantiateViewController(withIdentifier: "ChristusMuguerzaSur") as! ChristusMuguerzaSur
        self.navigationController?.pushViewController(pushController, animated: true)
    }
    
    @objc func push3()
    {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pushController = storyboard.instantiateViewController(withIdentifier: "HospitalSanJose") as! HospitalSanJose
        self.navigationController?.pushViewController(pushController, animated: true)
    }
    
    @objc func push4()
    {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pushController = storyboard.instantiateViewController(withIdentifier: "HospitalZambranoHellion") as! HospitalZambranoHellion
        self.navigationController?.pushViewController(pushController, animated: true)
    }
    
    @objc func push5()
    {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pushController = storyboard.instantiateViewController(withIdentifier: "SwissHospital") as! SwissHospital
        self.navigationController?.pushViewController(pushController, animated: true)
    }
    
    
    @objc func push6()
    {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pushController = storyboard.instantiateViewController(withIdentifier: "MembrasiaHospitales") as! MembrasiaHospitales
        self.navigationController?.pushViewController(pushController, animated: true)
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
