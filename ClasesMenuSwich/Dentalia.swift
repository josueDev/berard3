//
//  Dentalia.swift
//  Berard
//
//  Created by Josué Gustavo Hernández Villa on 13/02/19.
//  Copyright © 2019 SinergiaDigital. All rights reserved.
//

import UIKit

class Dentalia: UIViewController {
    
    
    @IBOutlet var botonDentalia1: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
         botonDentalia1.addTarget(self, action: #selector(self.push1), for: UIControl.Event.touchUpInside)
         botonDentalia1.imageView?.contentMode = .scaleAspectFit

        // Do any additional setup after loading the view.
    }
    
    @objc func push1()
    {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pushController = storyboard.instantiateViewController(withIdentifier: "DentaliaService") as! DentaliaService
        self.navigationController?.pushViewController(pushController, animated: true)
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
