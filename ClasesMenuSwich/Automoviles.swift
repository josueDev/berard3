//
//  Automoviles.swift
//  Berard
//
//  Created by Josué Gustavo Hernández Villa on 13/02/19.
//  Copyright © 2019 SinergiaDigital. All rights reserved.
//

import UIKit

class Automoviles: UIViewController {
    
    @IBOutlet var botonAuto1: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        botonAuto1.addTarget(self, action: #selector(self.push1), for: UIControl.Event.touchUpInside)
        botonAuto1.imageView?.contentMode = .scaleAspectFit

        }
        
        @objc func push1()
        {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let pushController = storyboard.instantiateViewController(withIdentifier: "BoschService") as! BoschService
            self.navigationController?.pushViewController(pushController, animated: true)
        }
    

}
