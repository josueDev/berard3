//
//  CeldaItem.swift
//  Berard
//
//  Created by Josué Gustavo Hernández Villa on 4/23/20.
//  Copyright © 2020 SinergiaDigital. All rights reserved.
//

import UIKit

class CeldaItem: UITableViewCell {
    
    @IBOutlet var title: UILabel!
    
    @IBOutlet var comp: UILabel!
    
    override func awakeFromNib() {
        
        title.numberOfLines = 2
        title.adjustsFontSizeToFitWidth = true
        
        
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
