//
//  CeldaPoliza.swift
//  Berard
//
//  Created by Josué Gustavo Hernández Villa on 26/02/19.
//  Copyright © 2019 SinergiaDigital. All rights reserved.
//

import UIKit

class CeldaPoliza: UITableViewCell {
    
    @IBOutlet var label1: UILabel!
    
    @IBOutlet var label2: UILabel!
    
    @IBOutlet var label3: UILabel!
    
    
    @IBOutlet var colorLabel: UILabel!
    
    
    @IBOutlet var colorImage: UIImageView!
    
    
    @IBOutlet var container: UIView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8))
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        
        container.layer.cornerRadius = 10
        container.clipsToBounds = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
