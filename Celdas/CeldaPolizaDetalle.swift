//
//  CeldaPolizaDetalle.swift
//  Berard
//
//  Created by Josué :D on 02/04/19.
//  Copyright © 2019 SinergiaDigital. All rights reserved.
//

import UIKit

class CeldaPolizaDetalle: UITableViewCell {
    
    @IBOutlet var colorLabel: UILabel!
    
    
    @IBOutlet var colorImage: UIImageView!
    
    
    @IBOutlet var container: UIView!
    @IBOutlet var labelDerecha: UILabel!
    
    @IBOutlet var labelIzquierda: UILabel!
    
    
    @IBOutlet var titulo1: UILabel!
    
    @IBOutlet var titulo2: UILabel!
    
    @IBOutlet var titulo3: UILabel!
    
    @IBOutlet var label1: UILabel!
    
    @IBOutlet var label2: UILabel!
    
    @IBOutlet var label3: UILabel!
    
    @IBOutlet var label4: UILabel!
    
    @IBOutlet var label5: UILabel!
    
    @IBOutlet var label6: UILabel!
    
    @IBOutlet var label7: UILabel!
    
    @IBOutlet var label8: UILabel!
    
    @IBOutlet var label9: UILabel!
    
    @IBOutlet var label10: UILabel!
    
    @IBOutlet var label11: UILabel!
    
    @IBOutlet var label12: UILabel!
    
    @IBOutlet var label13: UILabel!
    
  
    @IBOutlet var label14: UILabel!
    
    
    
    @IBOutlet var n1: UILabel!
    
    @IBOutlet var n2: UILabel!
    
    
    
    
    @IBOutlet var importe: UILabel!
    @IBOutlet var importeProteccion: UILabel!
    
    
    
    
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8))
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        container.layer.cornerRadius = 10
        container.clipsToBounds = true
        
        importe.adjustsFontSizeToFitWidth = true
        importeProteccion.adjustsFontSizeToFitWidth = true
        
        label1.adjustsFontSizeToFitWidth = true
        label2.adjustsFontSizeToFitWidth = true
        label3.adjustsFontSizeToFitWidth = true
        label4.adjustsFontSizeToFitWidth = true
        label5.adjustsFontSizeToFitWidth = true
        label6.adjustsFontSizeToFitWidth = true
        label7.adjustsFontSizeToFitWidth = true
        label8.adjustsFontSizeToFitWidth = true
        label9.adjustsFontSizeToFitWidth = true
        label10.adjustsFontSizeToFitWidth = true
        label11.adjustsFontSizeToFitWidth = true
        label12.adjustsFontSizeToFitWidth = true
        label13.adjustsFontSizeToFitWidth = true
        label14.adjustsFontSizeToFitWidth = true
        
        n1.adjustsFontSizeToFitWidth = true
        n2.adjustsFontSizeToFitWidth = true
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
