//
//  CollectionViewCell.swift
//  Berard
//
//  Created by Josué Gustavo Hernández Villa on 4/24/20.
//  Copyright © 2020 SinergiaDigital. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {

    @IBOutlet var title: UILabel!
    
    @IBOutlet var subtitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        title.adjustsFontSizeToFitWidth = true
        subtitle.adjustsFontSizeToFitWidth = true
        // Initialization code
    }

}
