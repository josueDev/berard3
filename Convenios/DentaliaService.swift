//
//  Dentalia.swift
//  Berard
//
//  Created by Josué Gustavo Hernández Villa on 4/21/20.
//  Copyright © 2020 SinergiaDigital. All rights reserved.
//

import UIKit


class DentaliaService: UIViewController {
    var backbutton = UIButton()
    
    @IBOutlet var imagen: UIImageView!
    
    @IBOutlet var label1: UILabel!
    
    @IBOutlet var label2: UILabel!
    
    @IBOutlet var label3: UILabel!
    
    @IBOutlet var label4: UILabel!
    
    @IBOutlet var label5: UILabel!
    
    @IBOutlet var label6: UILabel!
    
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let firstFrame = CGRect(x: 0, y: 0, width:1000, height: 45)
        let firstLabel = UILabel(frame: firstFrame)
        
        let nombre = String(describing: UserDefaults.standard.value(forKey: DataPersistent.nombre)!)
        firstLabel.text = "Hola \(nombre)"
        firstLabel.textAlignment = .center
        firstLabel.adjustsFontSizeToFitWidth = true
        self.navigationItem.titleView = firstLabel
        self.navigationController?.navigationBar.tintColor = UIColor(red: 255/255, green: 266/255, blue: 255/255, alpha: 1) // title and icon back
        navigationController?.navigationBar.barTintColor =  UIColor(red: 132/255, green: 25/255, blue: 19/255, alpha: 1)
        //self.navigationController!.navigationBar.titleTextAttributes = [ NSAttributedStringKey.foregroundColor: UIColor(red: 7/255, green: 7/255, blue: 7/255, alpha: 1)] // titleBar
        let leftBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "back"), style: .done, target: self, action: #selector(backmenu))
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
        //UIApplication.shared.statusBarStyle = .default
        
        let rightBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "power"), style: .done, target: self, action: #selector(backmenu))
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
       
        
        backbutton = UIButton(type: .system)
        backbutton.addTarget(self, action: #selector(gomenu), for: UIControl.Event.touchUpInside)
        backbutton.setImage(UIImage(named: "menubar.png"), for: .normal) // Image can be downloaded from here below link
        ///backbutton.setTitle("Back", forState: .Normal)
        backbutton.tintColor =  UIColor(red: 255/255, green: 266/255, blue: 255/255, alpha: 1)        //backbutton.setTitleColor(backbutton.tintColor, for: .normal) // You can change the TitleColor
        //backbutton.addTarget(self, action: "backAction", forControlEvents: .TouchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: backbutton)
        
        let image = UIImage(named: "white")!
        let imageSize = CGRect(x: 0, y: 0, width:1000, height: 25)
        let marginX: CGFloat = (self.navigationController!.navigationBar.frame.size.width / 2) - (imageSize.width / 2)
        let imageView = UIImageView(frame: CGRect(x: marginX, y: 0, width: imageSize.width, height: imageSize.height))
        imageView.image = image
        imageView.contentMode = .scaleAspectFit
        
        self.navigationItem.titleView = imageView
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let label1f1 = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16)]
        let label1f2 = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15)]
        let label1f3 = [NSAttributedString.Key.foregroundColor: UIColor.black,  NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Bold", size: 17)]
        let label1f4 = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15)]
        let label1f5 = [NSAttributedString.Key.foregroundColor: UIColor.black,  NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Bold", size: 15)]
        
        let label1t1 = NSMutableAttributedString(string: "GNP ", attributes: label1f1)
        let label1t2 = NSMutableAttributedString(string: "en convenio con ", attributes: label1f2)
        let label1t3 = NSMutableAttributedString(string: "DENTALIA", attributes: label1f3 as [NSAttributedString.Key : Any])
        let label1t4 = NSMutableAttributedString(string: " se unen para fortalecer la cobertura de tu seguro, que incluye los siguientes beneficios", attributes: label1f4)
        
        let combination = NSMutableAttributedString()
        
        combination.append(label1t1)
        combination.append(label1t2)
        combination.append(label1t3)
        combination.append(label1t4)
        
        label1.attributedText = combination
        label1.adjustsFontSizeToFitWidth = true
        
  
        
        let labef1d1 = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15)]
        let labef1d2 = [NSAttributedString.Key.foregroundColor: UIColor.black,  NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Bold", size: 15)]
          
        let label1d1 = NSMutableAttributedString(string: "DESCUENTOS \n\n", attributes: labef1d2 as [NSAttributedString.Key : Any])
        let label1d2 = NSMutableAttributedString(string: "\u{2022} 70% En consulta, resina, amalgamas y extracciones \n\n \u{2022} 50% En endodoncia, radiocetomía y pulpotomía \n\n \u{2022} 10% En endodoncia, blanqueamiento y coronas ", attributes: labef1d1)
        let label1d3 = NSMutableAttributedString(string: "\n\nPROCEDIMIENTOS GRATUITOS \n\n", attributes:  labef1d2 as [NSAttributedString.Key : Any])
        let label1d4 = NSMutableAttributedString(string: "\u{2022} Revisión, diagnóstico \n\n \u{2022} 2 Limpiezas \n\n \u{2022} Evaluación de especialiastas ", attributes: labef1d1)
        
        let combination3 = NSMutableAttributedString()
          
          combination3.append(label1d1)
          combination3.append(label1d2)
          combination3.append(label1d3)
          combination3.append(label1d4)
        
        
        //label2.text = combination2
        label2.attributedText = combination3
        label2.adjustsFontSizeToFitWidth = true
        
        
        label2.adjustsFontSizeToFitWidth = true
        
        let combination4 = NSMutableAttributedString()
        
        
        let labef1ft1 = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15)]
        let labef1ft2 = [NSAttributedString.Key.foregroundColor: UIColor.black,  NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Bold", size: 15)]
                 
        let label1te1 = NSMutableAttributedString(string: "Beneficio ", attributes: labef1ft1 as [NSAttributedString.Key : Any])
        let label1te2 = NSMutableAttributedString(string: "SIN COSTO ", attributes: labef1ft2 as [NSAttributedString.Key : Any])
        let label1te3 = NSMutableAttributedString(string: "dentro de la cobertura básica de tu póliza a partir del 1 de Diciembre de 2017", attributes:  labef1ft1 as [NSAttributedString.Key : Any])
        
        combination4.append(label1te1)
        combination4.append(label1te2)
        combination4.append(label1te3)
        
        label3.attributedText = combination4
        label3.adjustsFontSizeToFitWidth = true
        
        let combination2 = NSMutableAttributedString()
        let labef1ft1l5 = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14)]
        let label1t5 = NSMutableAttributedString(string: "", attributes: labef1ft1l5)
         combination2.append(label1t5)
        label4.attributedText = combination2
        
    
        label4.adjustsFontSizeToFitWidth = true
        label5.text = "Indispensable presentar identificación que los acredite como asegurados de Berard Seguros"
        label5.adjustsFontSizeToFitWidth = true
        label6.adjustsFontSizeToFitWidth = true
        // Do any additional setup after loading the view.
    }
    
    @objc func gomenu()
    {
        //UserDefaults.standard.setValue("true", forKey: DataPersistent.TERMINAL_LOGIN)
    }
    
    @objc func backmenu()
    {
//        for controller in self.navigationController!.viewControllers as Array {
//            if controller.isKind(of: Home.self) {
//                self.navigationController!.popToViewController(controller, animated: true)
//                break
//            }
//        }
        
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
