//
//  UbicaCamContainer.swift
//  Berard
//
//  Created by Josué Gustavo Hernández Villa on 4/23/20.
//  Copyright © 2020 SinergiaDigital. All rights reserved.
//

import UIKit

class UbicaCamContainer: UIViewController {
    
    
    @IBOutlet var l1: UILabel!
    
    @IBOutlet var l2: UILabel!
    
    @IBOutlet var l3: UILabel!
    
    @IBOutlet var l4: UILabel!
    
    @IBOutlet var img: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        l1.adjustsFontSizeToFitWidth = true
        l2.adjustsFontSizeToFitWidth = true
        l3.adjustsFontSizeToFitWidth = true
        l4.adjustsFontSizeToFitWidth = true

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
