//
//  ChristusMuguerza.swift
//  Berard
//
//  Created by Josué Gustavo Hernández Villa on 14/02/19.
//  Copyright © 2019 SinergiaDigital. All rights reserved.
//

import UIKit

class ChristusMuguerza: UIViewController {
    
    @IBOutlet var label1: UILabel!
    
    @IBOutlet var label2: UILabel!
    
    @IBOutlet var label3: UILabel!
    
    @IBOutlet var label4: UILabel!
    
    @IBOutlet var label5: UILabel!
    

    @IBOutlet var bottomlabel: UILabel!
    
    
    var backbutton = UIButton()
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let firstFrame = CGRect(x: 0, y: 0, width:1000, height: 45)
        let firstLabel = UILabel(frame: firstFrame)
        
        let nombre = String(describing: UserDefaults.standard.value(forKey: DataPersistent.nombre)!)
        firstLabel.text = "Hola \(nombre)"
        firstLabel.textAlignment = .center
        firstLabel.adjustsFontSizeToFitWidth = true
        self.navigationItem.titleView = firstLabel
        self.navigationController?.navigationBar.tintColor = UIColor(red: 255/255, green: 266/255, blue: 255/255, alpha: 1) // title and icon back
        navigationController?.navigationBar.barTintColor =  UIColor(red: 132/255, green: 25/255, blue: 19/255, alpha: 1)
        //self.navigationController!.navigationBar.titleTextAttributes = [ NSAttributedStringKey.foregroundColor: UIColor(red: 7/255, green: 7/255, blue: 7/255, alpha: 1)] // titleBar
        let leftBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "back"), style: .done, target: self, action: #selector(backmenu))
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
        //UIApplication.shared.statusBarStyle = .default
        
        let rightBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "power"), style: .done, target: self, action: #selector(backmenu))
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
        
        
        backbutton = UIButton(type: .system)
        backbutton.addTarget(self, action: #selector(gomenu), for: UIControl.Event.touchUpInside)
        backbutton.setImage(UIImage(named: "menubar.png"), for: .normal) // Image can be downloaded from here below link
        ///backbutton.setTitle("Back", forState: .Normal)
        backbutton.tintColor =  UIColor(red: 255/255, green: 266/255, blue: 255/255, alpha: 1)        //backbutton.setTitleColor(backbutton.tintColor, for: .normal) // You can change the TitleColor
        //backbutton.addTarget(self, action: "backAction", forControlEvents: .TouchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: backbutton)
        
        let image = UIImage(named: "white")!
        let imageSize = CGRect(x: 0, y: 0, width:1000, height: 25)
        let marginX: CGFloat = (self.navigationController!.navigationBar.frame.size.width / 2) - (imageSize.width / 2)
        let imageView = UIImageView(frame: CGRect(x: marginX, y: 0, width: imageSize.width, height: imageSize.height))
        imageView.image = image
        imageView.contentMode = .scaleAspectFit
        
        self.navigationItem.titleView = imageView
    }
    
    @objc func gomenu()
    {
        
        let result = String(describing: UserDefaults.standard.value(forKey: DataPersistent.TERMINAL_LOGIN)!)
        
        if result == "true"
        {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: Menu.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        }
        else
        {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: Home.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
            
        }
    }
    
    @objc func backmenu()
        
    {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let label1f1 = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16)]
        let label1f2 = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15)]
        let label1f3 = [NSAttributedString.Key.foregroundColor: UIColor.black,  NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Bold", size: 17)]
        let label1f4 = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15)]
        
        let label1t1 = NSMutableAttributedString(string: "BERARD SEGUROS ", attributes: label1f1)
        let label1t2 = NSMutableAttributedString(string: "en convenio con ", attributes: label1f2)
        let label1t3 = NSMutableAttributedString(string: "CHRISTUS MUGUERZA ALTA ESPECIALIDAD", attributes: label1f3 as [NSAttributedString.Key : Any])
        let label1t4 = NSMutableAttributedString(string: " ha creado un esquema de beneficios exclusivos adicionales a los que ya incluye tu póliza de Gastos Médicos Mayores Berard", attributes: label1f4)
        
        let combination = NSMutableAttributedString()
        
        combination.append(label1t1)
        combination.append(label1t2)
        combination.append(label1t3)
        combination.append(label1t4)
        
        label1.attributedText = combination
        label1.adjustsFontSizeToFitWidth = true
        
        label2.text = "\u{2022} Upgrade para casos especiales en los que Berard Seguros solicita el ascenso en el nivel de la habitación \n\n \u{2022} Codonación de $1,500 mn en deducible \n\n \u{2022} Paquetes de Maternidad a precios especiales \n\n \u{2022} $849,00 mn paquete Eco y/o Mamografía en el centro de mama \n\n Descuentos sin hacer uso de su póliza de: \n\n \u{2022} 20% de Descuentos en estudios de Laboratorio \n\n \u{2022} 20% de Descuento en estudios de Imagenología y Gabinete \n\n \u{2022} 20% de Descuento en Check Up \n\n \u{2022} $250,00 mn Consulta médica familiar en consultas"
         label2.adjustsFontSizeToFitWidth = true
        
        label3.text = "Aplica restricciones \n No aplica en estudios especiales,subrogados,paquetes,contrastes e insumos"
         label3.adjustsFontSizeToFitWidth = true
        
        let label1f3r = [NSAttributedString.Key.foregroundColor: UIColor.black,  NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Bold", size: 15)]
        //let label1t3r = NSMutableAttributedString(string: "CONTACTO \n Conmutador - (81)8399 3400 \n Admisión/Hospitalización - (81)8399 3430 \n Caja - (81)8399 3408 \n Consulta Externa - (81)8399 3408 \n Urgencias - (81)8399 3408 Ext. 3449", attributes: label1f3r as [NSAttributedString.Key : Any])
        //let label1t3r = NSMutableAttributedString(string: "CONTACTO ", attributes: label1f3r as [NSAttributedString.Key : Any])
         //let combination2 = NSMutableAttributedString()
         //combination.append(label1t3r)
        //label4.attributedText = combination2
        label4.adjustsFontSizeToFitWidth = true
        label4.text =  "CONTACTO \n Conmutador - (81)8399 3400 \n Admisión/Hospitalización - (81)8399 3430 \n Caja - (81)8399 3408 \n Consulta Externa - (81)8399 3408 \n Urgencias - (81)8399 3408 Ext. 3449"
        
        label5.text = "Insdispensable presentar identificación que los acredite como asegurados de Berard Seguros"
         label5.adjustsFontSizeToFitWidth = true
        
        bottomlabel.adjustsFontSizeToFitWidth = true
        
        
        
        

        // Do any additional setup after loading the view.
    }
    

    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

