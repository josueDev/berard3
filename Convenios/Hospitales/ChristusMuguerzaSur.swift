//
//  ChristusMuguerzaSur.swift
//  Berard
//
//  Created by Josué Gustavo Hernández Villa on 14/02/19.
//  Copyright © 2019 SinergiaDigital. All rights reserved.
//

import UIKit

class ChristusMuguerzaSur: UIViewController {
    
    
    @IBOutlet var image1: UIImageView!
    
    @IBOutlet var label1: UILabel!
    
    @IBOutlet var label2: UILabel!
    
    @IBOutlet var label3: UILabel!
    
    @IBOutlet var label4: UILabel!
    
    @IBOutlet var label5: UILabel!
    
    
    
    
    
    
    var backbutton = UIButton()
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let firstFrame = CGRect(x: 0, y: 0, width:1000, height: 45)
        let firstLabel = UILabel(frame: firstFrame)
        
        let nombre = String(describing: UserDefaults.standard.value(forKey: DataPersistent.nombre)!)
        firstLabel.text = "Hola \(nombre)"
        firstLabel.textAlignment = .center
        firstLabel.adjustsFontSizeToFitWidth = true
        self.navigationItem.titleView = firstLabel
        self.navigationController?.navigationBar.tintColor = UIColor(red: 255/255, green: 266/255, blue: 255/255, alpha: 1) // title and icon back
        navigationController?.navigationBar.barTintColor =  UIColor(red: 132/255, green: 25/255, blue: 19/255, alpha: 1)
        //self.navigationController!.navigationBar.titleTextAttributes = [ NSAttributedStringKey.foregroundColor: UIColor(red: 7/255, green: 7/255, blue: 7/255, alpha: 1)] // titleBar
        let leftBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "back"), style: .done, target: self, action: #selector(backmenu))
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
        //UIApplication.shared.statusBarStyle = .default
        
        let rightBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "power"), style: .done, target: self, action: #selector(backmenu))
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
        
        
        backbutton = UIButton(type: .system)
        backbutton.addTarget(self, action: #selector(gomenu), for: UIControl.Event.touchUpInside)
        backbutton.setImage(UIImage(named: "menubar.png"), for: .normal) // Image can be downloaded from here below link
        ///backbutton.setTitle("Back", forState: .Normal)
        backbutton.tintColor =  UIColor(red: 255/255, green: 266/255, blue: 255/255, alpha: 1)        //backbutton.setTitleColor(backbutton.tintColor, for: .normal) // You can change the TitleColor
        //backbutton.addTarget(self, action: "backAction", forControlEvents: .TouchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: backbutton)
        
        let image = UIImage(named: "white")!
        let imageSize = CGRect(x: 0, y: 0, width:1000, height: 25)
        let marginX: CGFloat = (self.navigationController!.navigationBar.frame.size.width / 2) - (imageSize.width / 2)
        let imageView = UIImageView(frame: CGRect(x: marginX, y: 0, width: imageSize.width, height: imageSize.height))
        imageView.image = image
        imageView.contentMode = .scaleAspectFit
        
        self.navigationItem.titleView = imageView
    }
    
    @objc func gomenu()
    {
        
        let result = String(describing: UserDefaults.standard.value(forKey: DataPersistent.TERMINAL_LOGIN)!)
        
        if result == "true"
        {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: Menu.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        }
        else
        {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: Home.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
            
        }
    }
    
    @objc func backmenu()
        
    {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let label1f1 = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16)]
        let label1f2 = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15)]
        let label1f3 = [NSAttributedString.Key.foregroundColor: UIColor.black,  NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Bold", size: 17)]
        let label1f4 = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15)]
        let label1f5 = [NSAttributedString.Key.foregroundColor: UIColor.black,  NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Bold", size: 15)]
        
        let label1t1 = NSMutableAttributedString(string: "BERARD SEGUROS ", attributes: label1f1)
        let label1t2 = NSMutableAttributedString(string: "en convenio con ", attributes: label1f2)
        let label1t3 = NSMutableAttributedString(string: "CHRISTUS MUGUERZA SUR", attributes: label1f3 as [NSAttributedString.Key : Any])
        let label1t4 = NSMutableAttributedString(string: " tiene un esquema de beneficios exclusivo solo por ser parte de BERARD", attributes: label1f4)
        
        let combination = NSMutableAttributedString()
        
        combination.append(label1t1)
        combination.append(label1t2)
        combination.append(label1t3)
        combination.append(label1t4)
        
        label1.attributedText = combination
        label1.adjustsFontSizeToFitWidth = true
        
        label2.text = "\u{2022} Condonación sobre el pago de deducible de $1,500 \n\n \u{2022} Consulta externa $240 (Médico Familiar o Pediatría según aplique) \n\n \u{2022} Aplicación de inyectables intramuscular SIN COSTO \n\n \u{2022} Toma de presión arterial sin costo \n\n Laboratorio 10% de descuento \n\n \u{2022} Imagenología 10% de descuento \n\n \u{2022} Check Up  2X1 \n\n \u{2022} Mamografía $500 \n\n \u{2022} Costo especial en paquetes de maternidad"
        label2.adjustsFontSizeToFitWidth = true
        
        let combination2 = NSMutableAttributedString()
        let label1t5 = NSMutableAttributedString(string: "CONTACTO \n Conmutador.- 8155 5000", attributes: label1f5)
        combination2.append(label1t5)
        
        label3.attributedText = combination2
        label3.adjustsFontSizeToFitWidth = true
        
        let lb1 = NSMutableAttributedString(string: "Insdispensable presentar identificación que los acredite como asegurados de ", attributes: label1f4)
        let lb2 = NSMutableAttributedString(string: "Berard Seguros", attributes: label1f5)
        
        let combination3 = NSMutableAttributedString()
        combination3.append(lb1)
        combination3.append(lb2)
        
        
        label4.attributedText = combination3
        label4.adjustsFontSizeToFitWidth = true
        //label5.text = "Insdispensable presentar identificación que los acredite como asegurados de Berard Seguros"
        label5.adjustsFontSizeToFitWidth = true
        

        // Do any additional setup after loading the view.
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
