//
//  HospitalZambranoHellion.swift
//  Berard
//
//  Created by Josué Gustavo Hernández Villa on 14/02/19.
//  Copyright © 2019 SinergiaDigital. All rights reserved.
//

import UIKit

class HospitalZambranoHellion: UIViewController {
    
    
    @IBOutlet var label1: UILabel!
    
    @IBOutlet var label2: UILabel!
    
    @IBOutlet var label3: UILabel!
    
    
    @IBOutlet var label4: UILabel!
    
    @IBOutlet var label5: UILabel!
    
    @IBOutlet var label6: UILabel!
    
    
    
    var backbutton = UIButton()
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let firstFrame = CGRect(x: 0, y: 0, width:1000, height: 45)
        let firstLabel = UILabel(frame: firstFrame)
        
        let nombre = String(describing: UserDefaults.standard.value(forKey: DataPersistent.nombre)!)
        firstLabel.text = "Hola \(nombre)"
        firstLabel.textAlignment = .center
        firstLabel.adjustsFontSizeToFitWidth = true
        self.navigationItem.titleView = firstLabel
        self.navigationController?.navigationBar.tintColor = UIColor(red: 255/255, green: 266/255, blue: 255/255, alpha: 1) // title and icon back
        navigationController?.navigationBar.barTintColor =  UIColor(red: 132/255, green: 25/255, blue: 19/255, alpha: 1)
        //self.navigationController!.navigationBar.titleTextAttributes = [ NSAttributedStringKey.foregroundColor: UIColor(red: 7/255, green: 7/255, blue: 7/255, alpha: 1)] // titleBar
        let leftBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "back"), style: .done, target: self, action: #selector(backmenu))
        self.navigationItem.leftBarButtonItem = leftBarButtonItem

        
        let rightBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "power"), style: .done, target: self, action: #selector(backmenu))
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
        //UIApplication.shared.statusBarStyle = .default
        
        backbutton = UIButton(type: .system)
        backbutton.addTarget(self, action: #selector(gomenu), for: UIControl.Event.touchUpInside)
        backbutton.setImage(UIImage(named: "menubar.png"), for: .normal) // Image can be downloaded from here below link
        ///backbutton.setTitle("Back", forState: .Normal)
        backbutton.tintColor =  UIColor(red: 255/255, green: 266/255, blue: 255/255, alpha: 1)        //backbutton.setTitleColor(backbutton.tintColor, for: .normal) // You can change the TitleColor
        //backbutton.addTarget(self, action: "backAction", forControlEvents: .TouchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: backbutton)
        
        let image = UIImage(named: "white")!
        let imageSize = CGRect(x: 0, y: 0, width:1000, height: 25)
        let marginX: CGFloat = (self.navigationController!.navigationBar.frame.size.width / 2) - (imageSize.width / 2)
        let imageView = UIImageView(frame: CGRect(x: marginX, y: 0, width: imageSize.width, height: imageSize.height))
        imageView.image = image
        imageView.contentMode = .scaleAspectFit
        
        self.navigationItem.titleView = imageView
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
           let label1f1 = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16)]
            let label1f2 = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15)]
            let label1f3 = [NSAttributedString.Key.foregroundColor: UIColor.black,  NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Bold", size: 17)]
            let label1f4 = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15)]
            let label1f5 = [NSAttributedString.Key.foregroundColor: UIColor.black,  NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Bold", size: 15)]
            
            let label1t1 = NSMutableAttributedString(string: "BERARD SEGUROS ", attributes: label1f1)
            let label1t2 = NSMutableAttributedString(string: "en convenio con ", attributes: label1f2)
            let label1t3 = NSMutableAttributedString(string: "HOSPITAL ZAMBRANO HELLION", attributes: label1f3 as [NSAttributedString.Key : Any])
            let label1t4 = NSMutableAttributedString(string: " ha creado un esquema de beneficios exclusivos para nuestros asegurados", attributes: label1f4)
            
            let combination = NSMutableAttributedString()
            
            combination.append(label1t1)
            combination.append(label1t2)
            combination.append(label1t3)
            combination.append(label1t4)
            
            label1.attributedText = combination
            label1.adjustsFontSizeToFitWidth = true
            
            label2.text = "\u{2022} Descuentos hasta $11,000 mn en gastos no cubiertos por la compañia aseguradora \n\n \u{2022} Pase extra gratis de estacionamiento (en caso de hospitalización) \n\n \u{2022} Kit de bienvenida de cortesía \n\n \u{2022} Precios preferenciales en paquetes de Maternidad* \n\n \u{2022} Consulta médica familiar $250.000 mn ** \n\n \u{2022} Codonación de deducible $1,600 en caso de hospitalización \n\n \u{2022} Codonación del 10% de coaseguro en programación de cirugías a través de BerardSeguros en pólizas: PREMIER 200, PREMIUM Y PLATINO \n\n \u{2022} En pólizas con plan INDIGO se elimina la penalización del 20% al atenderse en cualquiera de los hospitales ***  \n\n \u{2022} 10% de Descuento en los restaurantesde cada uno de los hospitales \n\n \u{2022} Servicio de diagnóstico de Laboratorio e Imagenología a precio preferencial"
            
            
            label2.adjustsFontSizeToFitWidth = true
            label3.text = "*Aplica restricciones, preguntar sobres costos en oficina\n** Aplica únicamente en Hospital Zambrano Hellion / Lunes a Domingo 8:00 a 20:00 hrs\n *** Aplica solamente en círugías o padecimientos programados mediente Berard Seguros"
            label3.adjustsFontSizeToFitWidth = true
            
            let combination2 = NSMutableAttributedString()
            let label1t5 = NSMutableAttributedString(string: "Línea de atención para asegurados Berard Seguros \nDirecto.- 8888 0088\n Conmutador.- 8888 0000 Ext. 3388\nCorreo.- servicio.pacientes@tecsalud.mx\nHorario.- 8:00 a 17:30 hrs ", attributes: label1f5)
             combination2.append(label1t5)
            label4.attributedText = combination2
            
        
            label4.adjustsFontSizeToFitWidth = true
            label6.text = "Indispensable presentar identificación que los acredite como asegurados de Berard Seguros"
            label5.adjustsFontSizeToFitWidth = true
            label6.adjustsFontSizeToFitWidth = true
            // Do any additional setup after loading the view.

        
    }
    
    @objc func gomenu()
    {
        //UserDefaults.standard.setValue("true", forKey: DataPersistent.TERMINAL_LOGIN)
    }
    
    @objc func backmenu()
    {
//        for controller in self.navigationController!.viewControllers as Array {
//            if controller.isKind(of: Home.self) {
//                self.navigationController!.popToViewController(controller, animated: true)
//                break
//            }
//        }
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
