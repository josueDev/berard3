//
//  TablaDosHospitales.swift
//  Berard
//
//  Created by Josué Gustavo Hernández Villa on 4/23/20.
//  Copyright © 2020 SinergiaDigital. All rights reserved.
//

import UIKit



class TablaDosHospitales: UIViewController,UITableViewDelegate , UITableViewDataSource {
    
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "CeldaItem", bundle: nil), forCellReuseIdentifier: "CeldaItem")
        
        
        
        let obje1 = ObjetoTablaHospitalesDos(texto: "Rehabilitación Física", subtexto: "15%")
        let obje2 = ObjetoTablaHospitalesDos(texto: "Camara Hiperbárica", subtexto: "15%")
        
        
        
        
        ObjetoTablaHospitalesDos.getData().add(obje1)
        ObjetoTablaHospitalesDos.getData().add(obje2)
        
        
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ObjetoTablaHospitales.getData().count
    }
    
    func tableView(_ tableView:UITableView, heightForRowAt indexPath:IndexPath)->CGFloat {
        
        return 45
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celdaItem:CeldaItem = self.tableView.dequeueReusableCell(withIdentifier: "CeldaItem", for: indexPath) as! CeldaItem
        
        let obj = ObjetoTablaHospitales.getData().object(at: indexPath.row) as! ObjetoTablaHospitales
        
        celdaItem.backgroundColor = UIColor.white
        celdaItem.selectionStyle = UITableViewCell.SelectionStyle.none
        celdaItem.title.text = obj.texto
        celdaItem.comp.text = obj.subtexto
        
        return celdaItem
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You selected cell #\(indexPath.row)!")
        tableView.deselectRow(at: indexPath as IndexPath, animated: true);
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
