//
//  TablaUnoHospitales.swift
//  Berard
//
//  Created by Josué Gustavo Hernández Villa on 4/23/20.
//  Copyright © 2020 SinergiaDigital. All rights reserved.
//

import UIKit

class TablaUnoHospitales: UIViewController,UITableViewDelegate , UITableViewDataSource {
    
    @IBOutlet var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
            tableView.dataSource = self
            tableView.delegate = self
            tableView.register(UINib(nibName: "CeldaItem", bundle: nil), forCellReuseIdentifier: "CeldaItem")
            
                       
        
        let obje1 = ObjetoTablaHospitales(texto: "Consulta General CAM’S", subtexto: "$240")
        let obje2 = ObjetoTablaHospitales(texto: "Consulta con Pediatra", subtexto: "$350")
        let obje3 = ObjetoTablaHospitales(texto: "Consulta con Especialistas", subtexto: "$400 (Desde)")
        let obje4 = ObjetoTablaHospitales(texto: "Consulta de Urgencias", subtexto: "$400")
        let obje5 = ObjetoTablaHospitales(texto: "Uso de Sala", subtexto: "$335")
        let obje6 = ObjetoTablaHospitales(texto: "Limpieza Dental", subtexto: "$335")
        let obje7 = ObjetoTablaHospitales(texto: "Traslado de Ambulancia en Urgencias", subtexto: "$800 (Desde)")
        let obje8 = ObjetoTablaHospitales(texto: "Laboratorio", subtexto: "25%")
        let obje9 = ObjetoTablaHospitales(texto: "Imagenología", subtexto: "15%")
        let obje10 = ObjetoTablaHospitales(texto: "Farmacia", subtexto: "10%")
        let obje11 = ObjetoTablaHospitales(texto: "Mamografías y Ecos", subtexto: "15%")
        let obje12 = ObjetoTablaHospitales(texto: "Urgencias", subtexto: "15%")
        let obje13 = ObjetoTablaHospitales(texto: "Toma de Presión", subtexto: "15%")
        let obje14 = ObjetoTablaHospitales(texto: "Aplicación de inyectables IM", subtexto: "15%")
        
        
        
        ObjetoTablaHospitales.getData().add(obje1)
        ObjetoTablaHospitales.getData().add(obje2)
        ObjetoTablaHospitales.getData().add(obje3)
        ObjetoTablaHospitales.getData().add(obje4)
        ObjetoTablaHospitales.getData().add(obje5)
        ObjetoTablaHospitales.getData().add(obje6)
        ObjetoTablaHospitales.getData().add(obje7)
        ObjetoTablaHospitales.getData().add(obje8)
        ObjetoTablaHospitales.getData().add(obje9)
        ObjetoTablaHospitales.getData().add(obje10)
        ObjetoTablaHospitales.getData().add(obje11)
        ObjetoTablaHospitales.getData().add(obje12)
        ObjetoTablaHospitales.getData().add(obje13)
        ObjetoTablaHospitales.getData().add(obje14)

        
        // Do any additional setup after loading the view.
    }
    
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return ObjetoTablaHospitales.getData().count
        }
        
        func tableView(_ tableView:UITableView, heightForRowAt indexPath:IndexPath)->CGFloat {
            
            return 45
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let celdaItem:CeldaItem = self.tableView.dequeueReusableCell(withIdentifier: "CeldaItem", for: indexPath) as! CeldaItem
            
            let obj = ObjetoTablaHospitales.getData().object(at: indexPath.row) as! ObjetoTablaHospitales
            
            celdaItem.backgroundColor = UIColor.white
            celdaItem.selectionStyle = UITableViewCell.SelectionStyle.none
            celdaItem.title.text = obj.texto
            celdaItem.comp.text = obj.subtexto

            return celdaItem
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            print("You selected cell #\(indexPath.row)!")
            tableView.deselectRow(at: indexPath as IndexPath, animated: true);
        
        }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

