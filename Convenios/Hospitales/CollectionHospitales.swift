//
//  CollectionHospitales.swift
//  Berard
//
//  Created by Josué Gustavo Hernández Villa on 4/24/20.
//  Copyright © 2020 SinergiaDigital. All rights reserved.
//

import UIKit

class CollectionHospitales: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    
    @IBOutlet var collectionView: UICollectionView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
           self.collectionView.register(UINib(nibName: "CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CollectionViewCell")
        
       


             

           let obje1 = ObjetoCollectionHospitales(texto: "San Pedro", subtexto: "(URGENCIAS 24Hrs) Av. San Pedro #301 esq. Río Sena Col. Del Valle, San Pedro Garza García Nuevo León, México Tel. 8335 7702")
           let obje2 = ObjetoCollectionHospitales(texto: "Huinalá", subtexto: "(Horario puede variar) Carretera Camino a Huinalá #100 Local-06 Apodaca Nuevo León, México Tel. 1872 9000")
           let obje3 = ObjetoCollectionHospitales(texto: "Leones", subtexto: "(Horario puede variar) Av. Gómez Morín #300 Local 1E Res. Valle Campestre, San Pedro Garza García, Nuevo León, México Tel. 8378 4073")
           let obje4 = ObjetoCollectionHospitales(texto: "Cumbres Elite", subtexto: "(URGENCIAS 24Hrs) Av. Paseo de los Leones #185 Col. Cumbres Elite, Monterrey Nuevo León, México Tel. 1095 0612")
           let obje5 = ObjetoCollectionHospitales(texto: "Gonzalitos", subtexto: "(Horario puede variar) Av. José Eleuterio Gzz Local 20-E Col. Jardines del Cerro, Monterrey Nuevo León, México Tel. 8333 5793")
           let obje6 = ObjetoCollectionHospitales(texto: "Sendero", subtexto: "(URGENCIAS 24Hrs) Av. Sendero Norte #803-B Col. Privadas de Anáhuac Francés, Escobedo, Nuevo León, México Tel. 1104 8944")
           let obje7 = ObjetoCollectionHospitales(texto: "Universidad", subtexto: "(Horario puede variar) Av. Universidad #101 Nte Res. Anáhuac, San Nicolás de los Garza Nuevo León, México Tel. 8376 3527")
           let obje8 = ObjetoCollectionHospitales(texto: "Contry", subtexto: "(URGENCIAS 24Hrs) Av. Eugenio Garza Sada #745 Local 27 Col. Contry, Monterrey Nuevo León, México Tel. 8365 6010")
           let obje9 = ObjetoCollectionHospitales(texto: "Escobedo", subtexto: "(Horario puede variar) Av. Las Torres #100 Col. Nuestra Sra. de Fatima, Escobedo Nuevo León, México Tel. 8307 4444")
           let obje10 = ObjetoCollectionHospitales(texto: "Chapultepec", subtexto: "(URGENCIAS 24Hrs) Av. Chapultepec #1800 Ote. Local B-04 Col. Paraíso, Guadalupe Nuevo León, México Tel. 8191 9424")
     
           
           ObjetoCollectionHospitales.getData().add(obje1)
           ObjetoCollectionHospitales.getData().add(obje2)
           ObjetoCollectionHospitales.getData().add(obje3)
           ObjetoCollectionHospitales.getData().add(obje4)
           ObjetoCollectionHospitales.getData().add(obje5)
           ObjetoCollectionHospitales.getData().add(obje6)
           ObjetoCollectionHospitales.getData().add(obje7)
           ObjetoCollectionHospitales.getData().add(obje8)
           ObjetoCollectionHospitales.getData().add(obje9)
           ObjetoCollectionHospitales.getData().add(obje10)

        
        let cellWidth : CGFloat = collectionView.frame.size.width / 2.0
           let cellheight : CGFloat = 100
           let cellSize = CGSize(width: cellWidth , height:cellheight)

           let layout = UICollectionViewFlowLayout()
           layout.scrollDirection = .vertical //.horizontal
           layout.itemSize = cellSize
           layout.sectionInset = UIEdgeInsets(top: 30, left: 1, bottom: 30, right: 1)
           layout.minimumLineSpacing = 1.0
           layout.minimumInteritemSpacing = 1.0
           collectionView.setCollectionViewLayout(layout, animated: true)

           collectionView.reloadData()
        
       // collectionView.collectionViewLayout.invalidateLayout()
        

    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        ObjetoCollectionHospitales.getData().count
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
          return 1
      }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
         let obj = ObjetoCollectionHospitales.getData().object(at: indexPath.row) as! ObjetoCollectionHospitales
        
        let cell : CollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as! CollectionViewCell

        cell.title.text = obj.texto
        cell.subtitle.text = obj.subtexto
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
            return UIEdgeInsets(top: 1.0, left: 1.0, bottom: 30.0, right: 1.0)//here your custom value for spacing
        }
    

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
