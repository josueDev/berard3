//
//  ObjectFavorites.swift
//  TASXI
//
//  Created by Josue Hernandez on 9/14/18.
//  Copyright © 2018 SinergiaDigital. All rights reserved.
//


import UIKit

private var array: NSMutableArray = NSMutableArray()

class ObjetoPoliza: NSObject {
    
    var id: String
    var titulo: String
    var poliza : String
    var compañia : String
    var vigencia : String
    var cobertura : String
    var estatus : String
    var tipo : String
    
    init( id : String,titulo : String, poliza : String, compañia : String, vigencia : String, cobertura : String, estatus : String , tipo : String) {
        
        self.id = id
        self.titulo = titulo
        self.poliza = poliza
        
        self.compañia = compañia
        self.vigencia = vigencia
        
        self.cobertura = cobertura
        
        self.estatus = estatus
        self.tipo = tipo
        
        
    }
    class func getData() -> NSMutableArray {
        return array
    }
    
    class func setData( listac : NSMutableArray ) {
        array = listac
    }
}
