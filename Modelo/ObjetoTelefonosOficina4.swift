//
//  ObjetoTelefonosOficina.swift
//  Berard
//
//  Created by Josué :D on 28/03/19.
//  Copyright © 2019 SinergiaDigital. All rights reserved.
//

import UIKit

private var array: NSMutableArray = NSMutableArray()

class ObjetoTelefonosOficina4: NSObject {
    
    var id: String
    var imagen: UIImage
    var texto : String
    var telefono : String
    var tipo : Int
    
    init( id : String,imagen : UIImage, texto : String, telefono : String, tipo : Int) {
        
        self.id = id
        self.imagen = imagen
        self.texto = texto
        self.telefono = telefono
        self.tipo = tipo
    }
    class func getData() -> NSMutableArray {
        return array
    }
    
    class func setData( listac : NSMutableArray ) {
        array = listac
    }
}
