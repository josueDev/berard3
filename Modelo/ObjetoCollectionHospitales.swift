//
//  ObjetoCollectionHospitales.swift
//  Berard
//
//  Created by Josué Gustavo Hernández Villa on 4/24/20.
//  Copyright © 2020 SinergiaDigital. All rights reserved.
//

import UIKit

private var array: NSMutableArray = NSMutableArray()

class ObjetoCollectionHospitales: NSObject {
    
    var texto: String
    var subtexto: String

    
    init( texto : String,subtexto : String) {
        
        self.texto = texto
        self.subtexto = subtexto
    }
    class func getData() -> NSMutableArray {
        return array
    }
    
    class func setData( listac : NSMutableArray ) {
        array = listac
    }
}
