//
//  ObjetoPolizaDetalle.swift
//  Berard
//
//  Created by Josué Gustavo Hernández Villa on 25/04/19.
//  Copyright © 2019 SinergiaDigital. All rights reserved.
//

import UIKit

private var array: NSMutableArray = NSMutableArray()

class ObjetoPolizaDetalle: NSObject {
    
    var id: String
    var l1: String
    var l2 : String
    var l3 : String
    var l4 : String
    var l5 : String
    var l6 : String
    var l7 : String
    var l8 : String
    var l9 : String
    var l10 : String
    var l11 : String
    var l12 : String
    var l13 : String
    var l14 : String
    
    init( id : String,l1 : String, l2 : String, l3 : String, l4 : String, l5 : String, l6 : String , l7 : String , l8 : String, l9 : String, l10 : String, l11 : String, l12 : String, l13 : String, l14 : String) {
        
        self.id = id
        self.l1 = l1
        self.l2 = l2
        self.l3 = l3
        self.l4 = l4
        self.l5 = l5
        self.l6 = l6
        self.l7 = l7
        self.l8 = l8
        self.l9 = l9
        self.l10 = l10
        self.l11 = l11
        self.l12 = l12
        self.l13 = l13
        self.l14 = l14
        
        
        
    }
    class func getData() -> NSMutableArray {
        return array
    }
    
    class func setData( listac : NSMutableArray ) {
        array = listac
    }
}
