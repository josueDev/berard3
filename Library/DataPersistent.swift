//
//  DataPersistent.swift
//  x24
//
//  Created by Josué :D on 27/02/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import Foundation

class DataPersistent: NSObject {
    
    static let TERMINAL_INITIALIZE = "TERMINAL_INITIALIZE"
    static let TERMINAL_LOGIN = "TERMINAL_LOGIN"
    
    static let nombre = "nombre"
    static let edad = "edad"
    static let ciudad = "ciudad"
    static let email = "email"
    static let apellidos = "apellidos"
    static let colonia = "colonia"
    static let cp = "cp"
    static let estado = "estado"
    static let id = "id"
    static let password = "password"
    static let calle = "calle"
    static let foto = "foto"
    static let sexo = "sexo"
    static let municipio = "municipio"
    static let telefono = "telefono"
    static let imageUrl = ""
    
    static let id_viaje = "id_viaje"
    static let status_viaje = "status_viaje"
    static let initTravel = "initTravel"
    static let finishTravel = "finishTravel"
    
    static let initTravelLatitude = "initTravelLatitude"
    static let initTravelLongitude = "initTravelLongitude"
    static let finishTravelLatitude = "finishTravelLatitude"
    static let finishTravelLongitude = "finishTravelLongitude"
    static let id_viaje_pendiente = "id_viaje_pendiente"
    
    
    public func initializeDataPersistent()
    {
        let result = String(describing: UserDefaults.standard.value(forKey: DataPersistent.TERMINAL_INITIALIZE))
        print(result)
        if result == "nil"
        {
            
            UserDefaults.standard.setValue("true", forKey: DataPersistent.TERMINAL_INITIALIZE)
            
            UserDefaults.standard.setValue("false", forKey: DataPersistent.TERMINAL_LOGIN)
            
            UserDefaults.standard.setValue("", forKey: DataPersistent.nombre)
            UserDefaults.standard.setValue("", forKey: DataPersistent.edad)
            UserDefaults.standard.setValue("", forKey: DataPersistent.ciudad)
            UserDefaults.standard.setValue("", forKey: DataPersistent.email)
            UserDefaults.standard.setValue("", forKey: DataPersistent.apellidos)
            UserDefaults.standard.setValue("", forKey: DataPersistent.colonia)
            UserDefaults.standard.setValue("", forKey: DataPersistent.cp)
            UserDefaults.standard.setValue("", forKey: DataPersistent.estado)
            UserDefaults.standard.setValue("", forKey: DataPersistent.id)
            UserDefaults.standard.setValue("", forKey: DataPersistent.password)
            UserDefaults.standard.setValue("", forKey: DataPersistent.calle)
            UserDefaults.standard.setValue("", forKey: DataPersistent.foto)
            UserDefaults.standard.setValue("", forKey: DataPersistent.sexo)
            UserDefaults.standard.setValue("", forKey: DataPersistent.municipio)
            UserDefaults.standard.setValue("", forKey: DataPersistent.telefono)
            UserDefaults.standard.setValue("", forKey: DataPersistent.imageUrl)
            
            UserDefaults.standard.setValue("", forKey: DataPersistent.id_viaje)
            UserDefaults.standard.setValue("", forKey: DataPersistent.status_viaje)
            
            UserDefaults.standard.setValue("", forKey: DataPersistent.initTravel)
            UserDefaults.standard.setValue("", forKey: DataPersistent.finishTravel)
            
            UserDefaults.standard.setValue("", forKey: DataPersistent.initTravelLatitude)
            UserDefaults.standard.setValue("", forKey: DataPersistent.initTravelLongitude)
            UserDefaults.standard.setValue("", forKey: DataPersistent.finishTravelLatitude)
            UserDefaults.standard.setValue("", forKey: DataPersistent.finishTravelLongitude)
            
            UserDefaults.standard.setValue("", forKey: DataPersistent.id_viaje_pendiente)
            
            
        }
    }
}



//"token": "398e92b9bf4903abd27f3c6d0bbca82d5b26b64e",
//"username": "test",
//"email": "test@test.com",
//"id": "eea4d53d-1003-4337-b2d7-5a51f4a17259",
//"thumbnail": "/media/CACHE/images/users/test/3a764b160b52ed5cb8a829dc64ac1bd8.jpg",
//"first_name": "test",
//"last_name": "test test"

