//
//  PickTabla.swift
//  MMRent
//
import UIKit

var selectPicker = String()

class PickTabla: NSObject, UITableViewDelegate, UITableViewDataSource {

    
    
    var fondoGris : UIView!
 
    var myPicker = UITableView()
    var posicion : NSInteger = 0
    
    var viewControllerG = UIViewController()
    var data : NSArray!
    var eleccion : Int = 0
    var botonG : UIButton!
    
    init( datos : NSArray) {
        self.data = datos
        print(self.data.count)
        
        let nib = UINib(nibName: "CeldaPick", bundle: nil)
        myPicker.register(nib,forCellReuseIdentifier: "cellpick")

    }
    
    func showPickerr( ancho : CGFloat, viewcontroller : UIViewController , x : CGFloat, y : CGFloat , boton : UIButton ){
        
        botonG = boton

        myPicker.dataSource = self
        myPicker.delegate = self

        myPicker.frame = CGRectMake(x , y + 50, ancho, 45 * 3)
        myPicker.backgroundColor = UIColor.white
        myPicker.layer.zPosition = viewcontroller.view.layer.zPosition + 1
        
        myPicker.layer.borderWidth = 1
        //myPicker.layer.borderColor = colorGlobal.CGColor
        
        viewcontroller.view.addSubview(myPicker)
        
        myPicker.alpha = 0
//        let transitionOptions = UIViewAnimationOptions.TransitionNone
//        UIView.transitionWithView(viewcontroller.view, duration: 0.4, options: transitionOptions, animations: {
//
            self.myPicker.alpha = 0

//            }, completion: { finished in
//
//        })
        

        
        
        self.mostrarPicker()
        
    }
    
    func mostrarPicker(){
//        let transitionOptions = UIViewAnimationOptions.TransitionNone
//
//
//        UIView.animate(withDuration: 2.0, animations: {
//            let camera = GMSCameraPosition.camera(withTarget: newLocation.coordinate, zoom: zoom)
//            self.mapView.animate(to: camera)
            //self.mapView.reloadInputViews()
            
            //self.mapView.hitTest(CGPoint(x: 400, y: 400), with: nil)
            //self.mapView.hitTest(CGPoint(x: 400, y: 400), with: nil)
            
            //let point = CGPoint(x: 0.0, y: (textView.contentSize.height - textView.bounds.height))
            //self.mapView.scroll
       // })
            
            self.myPicker.alpha = 1
            //self.fondoPick.frame = CGRectMake(0 , CGRectGetHeight(self.fondoGris.frame) - 230, CGRectGetWidth(self.fondoGris.frame), 230)
            //self.myPicker.frame = CGRectMake(0 , 230, self.fondoGris.frame.width, 230)
            
            

    }
    
    func hidePicker(){
        
        if self.myPicker.frame.height != 0
        {
            self.myPicker.alpha = 1
//            let transitionOptions = UIViewAnimationOptions.TransitionNone
//            UIView.transitionWithView(viewControllerG.view, duration: 0.4, options: transitionOptions, animations: {
            
                self.myPicker.alpha = 0
                //self.fondoPick.frame = CGRectMake(0 , CGRectGetHeight(self.fondoGris.frame) - 230, CGRectGetWidth(self.fondoGris.frame), 230)
                //self.myPicker.frame = CGRectMake(0 , 230, self.fondoGris.frame.width, 230)
                
                
//                }, completion: { finished in
//                    self.myPicker.removeFromSuperview()
//            })
        }
    }
    


    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView:UITableView, heightForRowAt indexPath:IndexPath)->CGFloat {
        
        return 45
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellpick:CeldaPick = self.myPicker.dequeueReusableCell(withIdentifier: "cellpick", for: indexPath) as! CeldaPick
        
        cellpick.backgroundColor = UIColor.white
        cellpick.selectionStyle = UITableViewCell.SelectionStyle.none
        
        cellpick.texto.text = data[indexPath.row] as? String
        
        
        return cellpick
    }
    

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You selected cell #\(indexPath.row)!")
        tableView.deselectRow(at: indexPath as IndexPath, animated: true);
        
        selectPicker = data[indexPath.row] as! String
        //botonG.setTitle(selectPicker, for: UIControlState.Normal)
        botonG.setTitle(selectPicker, for: .normal)
        
        
        self.myPicker.alpha = 1
//        let transitionOptions = UIViewAnimationOptions.TransitionNone
//        UIView.transitionWithView(viewControllerG.view, duration: 0.4, options: transitionOptions, animations: {
//
            self.myPicker.alpha = 0
            //self.fondoPick.frame = CGRectMake(0 , CGRectGetHeight(self.fondoGris.frame) - 230, CGRectGetWidth(self.fondoGris.frame), 230)
            //self.myPicker.frame = CGRectMake(0 , 230, self.fondoGris.frame.width, 230)
            
            
            //}, completion: { finished in
                self.myPicker.removeFromSuperview()
       // })
        
        
        
        
    }
    

    func CGRectMake(_ x: CGFloat, _ y: CGFloat, _ width: CGFloat, _ height: CGFloat) -> CGRect {
        return CGRect(x: x, y: y, width: width, height: height)
    }
    
}

extension String
{
    func getSelectPicker() -> String
    {
        return selectPicker
    }
}


